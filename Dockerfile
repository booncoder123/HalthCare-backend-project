# Use the official Node.js image as the base image
FROM node:19-alpine3.16

# Create a directory for the app
WORKDIR /app

# Copy the package.json and package-lock.json files to the container
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application files to the container
COPY . .

# Expose the port on which the app will run
EXPOSE 3001

# Start the app
CMD ["npm", "start"]