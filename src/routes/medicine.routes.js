import express from "express";

import {
  postMedicine,
  getAllMedicine,
  getAllMedicineByPharmacyId,
  getMeidicineById,
  putMedicineById,
  deleteMedicineById,
  searchMedicine,
} from "../controllers/medicine.controller.js";

const router = express.Router();

router.post("/medicine", postMedicine);

router.get("/medicine", getAllMedicine);
router.get("/medicine/pharmacy/:id", getAllMedicineByPharmacyId);
router.get("/medicine/:id", getMeidicineById);
router.get("/medicine/search/:keyword", searchMedicine);

router.put("/medicine/:id", putMedicineById);

router.delete("/medicine/:id", deleteMedicineById);

export default router;
