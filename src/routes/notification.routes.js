import express from "express";
import multer from "multer";
import authMiddleware from "../middlewares/Auth.middleware.js";
import {
  createNotification,
  getNotification,
  readNotification,
  deleteNotification,
  getNotificationById,
  createNotificationToken,
  updateNotificationToken,
  serverSendNotificationToUser,
  userSendNotificationToUser,
  getNotificationByUserId

} from "../controllers/notification.controller.js";

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg" ||
    file.mimetype === "image/jpeg"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const router = express.Router();

router.post("/", authMiddleware, createNotification);

router.get("/user/:userId", authMiddleware, getNotification);

router.post("/token/read", authMiddleware, readNotification);

router.delete("/token", authMiddleware, deleteNotification);

router.get("/:id", authMiddleware, getNotificationById);

router.put("/notification/server/send/:receiver", serverSendNotificationToUser);

router.put("/notification/:sender/send/:receiver", userSendNotificationToUser);


router.put("/notification",authMiddleware, getNotificationByUserId);



export default router;
