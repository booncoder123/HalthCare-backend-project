import express from "express";
import multer from "multer";
import { createGroup, readGroup } from "../controllers/group.controllers.js";

const router = express.Router();

router.post("/chat/group", createGroup);

router.get("/chat/group", readGroup);

export default router;
