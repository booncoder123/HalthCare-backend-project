import express from "express";

import { getAllPharmacyAddresses } from "../controllers/address.controller.js";

import authMiddleware from "../middlewares/Auth.middleware.js";

const router = express.Router();

router.get("/map/pharmacies", getAllPharmacyAddresses);

export default router;
