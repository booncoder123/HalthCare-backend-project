import express from "express";
import multer from "multer";

import {
  deleteUserById,
  getAllUser,
  getUserById,
  getUserByUid,
  postUser,
  putUserById,
} from "../controllers/user.controller.js";

import authMiddleware from "../middlewares/Auth.middleware.js";

// import authMiddleware from "../../middlewares/Auth.middleware";
// import userService from "../../services/user.service";

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg" ||
    file.mimetype === "image/jpeg"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const router = express.Router();

router.post(
  "/user",
  multer({ dest: "temp/", fileFilter }).fields([
    {
      name: "faceImg",
      maxCount: 1,
    },
  ]),
  authMiddleware,
  postUser
);

router.get("/users", getAllUser);
router.get("/user/:id", getUserById);
router.get("/user", authMiddleware, getUserByUid);
router.get("/user/uid/:uid", getUserByUid);

router.put(
  "/user",
  multer({ dest: "temp/", fileFilter }).fields([
    {
      name: "faceImg",
      maxCount: 1,
    },
  ]),
  authMiddleware,
  putUserById
);

router.delete("/user/:id", deleteUserById);

// router.get("/user-detail", authMiddleware, userService.getDetail);

export default router;
