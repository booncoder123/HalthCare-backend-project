import express from "express";
import multer from "multer";
import {
  createMessage,
  readMessage,
} from "../controllers/message.controller.js";

const router = express.Router();

router.post("/chat/message", createMessage);

router.get("/chat/message", readMessage);

export default router;
