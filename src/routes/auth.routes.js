import express from "express";
import multer from "multer";
import { register, emailExist } from "../controllers/auth.controller.js";

const router = express.Router();

router.post("/register", register);

router.post("/register/email", emailExist);

export default router;
