import express from "express";

import {
  postJob,
  cancelJob,
  acceptJob,
  doneJob,
  getAllJob,
  getAllJobByPharmacistTok,
  getJobRequesterById,
  getPharmacistByJobId,
  getJobReceiverById,
  putIncreaseRadiusOfHospital,
  putJobStatus,
} from "../controllers/job.controller.js";

import authMiddleware from "../middlewares/Auth.middleware.js";

const router = express.Router();

router.post("/job", authMiddleware, postJob);
router.post("/job/cancel", cancelJob);
router.post("/job/accept", authMiddleware, acceptJob);
router.post("/job/done", doneJob);

router.get("/jobs", getAllJob);
router.get("/job/pharmacist/tok", authMiddleware, getAllJobByPharmacistTok);
router.get("/job/requester/:id", getJobRequesterById);
router.get("/job/receiver/:id", getJobReceiverById);
router.get("/job/pharmacist/:id", getPharmacistByJobId);

router.put("/job", putIncreaseRadiusOfHospital);
router.put("/job/status", putJobStatus);

export default router;
