import express from "express";
import multer from "multer";
import { createUser, readUser } from "../controllers/user.chat.controller.js";

const router = express.Router();

router.post("/chat/user", createUser);

router.get("/chat/user", readUser);

export default router;
