import express from "express";
import multer from "multer";

import {
  postEmergencyCase,
  getAllEmergencyCase,
  getEmergencyCaseByUserId,
  getEmergencyCaseById,
  putEmergencyCaseById,
  deleteEmergencyCaseById,
  getEmergencyCaseByUid,
  getEmergencyCaseByHospitalId,
  putEmergencyCaseByUid,
  putEmergencyCaseStatusById,
  getEmergencyCaseAndJobById,
  getEmergencyCaseAndOrderByTokUserId,
  getEmergencyCaseByJobIdAndTok,
  getEmergencyCaseByJobId,
  getEmergencyCaseAndMedInfoById,
} from "../controllers/emergencyCase.controller.js";
import authMiddleware from "../middlewares/Auth.middleware.js";

const router = express.Router();

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg" ||
    file.mimetype === "image/jpeg"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

router.post(
  "/emergency/case",
  multer({ dest: "temp/", fileFilter }).fields([
    {
      name: "images",
      maxCount: 3,
    },
  ]),
  authMiddleware,
  postEmergencyCase
);

router.get("/emergency/cases", getAllEmergencyCase);
router.get("/emergency/case/user/:userid", getEmergencyCaseByUserId);
router.get("/emergency/case/:id", getEmergencyCaseById);
router.get("/emergency/case/job/:id", getEmergencyCaseAndJobById);
router.get("/emergency/case", authMiddleware, getEmergencyCaseByUid);
router.get("/emergency/case/hospital/:id", getEmergencyCaseByHospitalId);
router.get(
  "/emergency/case/order/tok",
  authMiddleware,
  getEmergencyCaseAndOrderByTokUserId
);
router.get(
  "/emergency/case/tok/job/:id",
  authMiddleware,
  getEmergencyCaseByJobIdAndTok
);
router.get(
  "/emergency/case/medical/information/:id",
  authMiddleware,
  getEmergencyCaseAndMedInfoById
);

router.get("/emergency/case/job/id/:jobId", getEmergencyCaseByJobId);

router.put("/emergency/case/:id", putEmergencyCaseById);
router.put("/emergency/case", authMiddleware, putEmergencyCaseByUid);
router.put("/emergency/case/status/:id", putEmergencyCaseStatusById);

router.delete("/emergency/case/:id", deleteEmergencyCaseById);

export default router;
