import express from "express";

import {
  postOrder,
  getAllOrder,
  getOrderById,
  getOrderByTokUserId,
  getOrderByTokPharmacistId,
  getOrderByUserId,
  putOrderById,
  deleteOrderById,
  getOrderByPharmacistId,
} from "../controllers/order.controller.js";
import authMiddleware from "../middlewares/Auth.middleware.js";

const router = express.Router();

router.post("/order", authMiddleware, postOrder);

router.get("/orders", authMiddleware, getAllOrder);
router.get("/order/:id", getOrderById);
router.get("/order/tok/user", authMiddleware, getOrderByTokUserId);
router.get("/order/tok/pharmacist", authMiddleware, getOrderByTokPharmacistId);
router.get("/order/user/:id", getOrderByUserId);
router.get("/order/pharmacist/:id", getOrderByPharmacistId);

router.put("/order/:id", putOrderById);

router.delete("/order/:id", deleteOrderById);

export default router;
