import express from "express";

import multer from "multer";


import { postCoordByUid, postImages } from "../controllers/trucks.controller.js";
import authMiddleware from "../middlewares/Auth.middleware.js";

const router = express.Router();

const fileFilter = (req, file, cb) => {
    if (
      file.mimetype === "image/png" ||
      file.mimetype === "image/jpg" ||
      file.mimetype === "image/jpeg"
    ) {
      cb(null, true);
    } else {
      cb(null, false);
    }
  };

router.post("/trucks/coord", authMiddleware, postCoordByUid);

router.post("/image/send",
multer({ dest: "temp/", fileFilter }).fields([
  {
    name: "images",
    maxCount: 10,
  },
]), authMiddleware, postImages);


export default router;
