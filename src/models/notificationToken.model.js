import mongoose from 'mongoose';

const { Schema } = mongoose;

const notificationTokenSchema = new Schema(
  {
    token: {
      type: String,
    },
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    }
  },
  { timestamps: true },
);

export default mongoose.model('NotificationToken', notificationTokenSchema);
