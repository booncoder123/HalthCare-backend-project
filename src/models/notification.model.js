import mongoose from "mongoose";

const { Schema } = mongoose;

const notificationSchema = new Schema(
  {
    type: {
      type: String,
    },
    title: {
      type: String,
    },
    isRead: {
      type: Boolean,
      default: false,
    },
    isDelete: {
      type: String,
      default: false,
    },
    body: {
      type: String,
    },
    feature: {
      type: String,
    },
    groupId: {
      type: String,
    },
    userId: {
      type: Schema.Types.ObjectId,
    },
  },
  { timestamps: true }
);

export default mongoose.model("Notification", notificationSchema);
