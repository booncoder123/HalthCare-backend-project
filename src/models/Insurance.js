import mongoose from "mongoose";

const Insurance = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Types.ObjectId,
    },
    insuranceNumber: {
      type: String,
    },
    provider: {
      type: String,
    },
    plan: {
      type: String,
    },
    expirationDate: {
      type: String,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  {
    strict: false,
    versionKey: false,
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);

export default mongoose.model("insurance", Insurance);
