import mongoose from "mongoose";

const Job = new mongoose.Schema(
  {
    requester: {
      type: String,
    },
    receiver: {
      type: String,
    },
    status: {
      type: String,
      default: "finding",
    },
    type: {
      type: String,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  {
    strict: false,
    versionKey: false,
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);

export default mongoose.model("job", Job);
