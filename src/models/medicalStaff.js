import mongoose from "mongoose";

const MedicalStaff = new mongoose.Schema(
  {
    hospitalId: {
      type: mongoose.Types.ObjectId,
    },
    userId: {
      type: mongoose.Types.ObjectId,
    },
    licenseId: {
      type: String,
    },
    licenseExpireDate: {
      type: String,
    },
    role: {
      type: String,
    },
    isOnDuty: {
      type: Boolean,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  {
    strict: false,
    versionKey: false,
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);

export default mongoose.model("medical staff", MedicalStaff);
