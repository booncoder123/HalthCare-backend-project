import mongoose from "mongoose";

const Order = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Types.ObjectId,
    },
    pharmacistId: {
      type: mongoose.Types.ObjectId,
    },
    jobId: {
      type: mongoose.Types.ObjectId,
    },
    medicines: [
      {
        id: { type: mongoose.Types.ObjectId },
        pharmacyId: {
          type: mongoose.Types.ObjectId,
        },
        name: {
          type: String,
        },
        dosage: {
          type: String,
        },
        duration: {
          type: String,
        },
        price: {
          type: Number,
        },
      },
    ],
    status: {
      type: String,
      default: false,
    },
    deliveryFee: {
      type: Number,
    },
    total: {
      type: Number,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  {
    strict: false,
    versionKey: false,
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);

export default mongoose.model("order", Order);
