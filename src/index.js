import express from "express";
import multer from "multer";
import cors from "cors";
import config from "../src/config/index.js";
import connectMongo from "./loaders/mongoDB.js";
import addressRouter from "./routes/address.routes.js";
import emergencyCaseRouter from "./routes/emergencyCase.routes.js";
import emergencyContactRouter from "./routes/emergencyContact.routes.js";
import hospitalRouter from "./routes/hospital.routes.js";
import insuranceRouter from "./routes/insurance.routes.js";
import medicalInformationRouter from "./routes/medicalInformation.routes.js";
import medicalStaffRouter from "./routes/medicalStaff.routes.js";
import pharmacistRouter from "./routes/pharmacist.routes.js";
import pharmacyRouter from "./routes/pharmacy.routes.js";
import productRouter from "./routes/product.routes.js";
import orderRouter from "./routes/order.routes.js";
import userRouter from "./routes/user.routes.js";
import authRouter from "./routes/auth.routes.js";
import chatRouter from "./routes/group.routes.js";
import medicineRouter from "./routes/medicine.routes.js";
import messageRouter from "./routes/message.routes.js";
import userChatRouter from "./routes/user.chat.routes.js";
import ErrorHandler from "./middlewares/ErrorHandler.js";
import connectQueue from "./loaders/queue.js";
import jobRouter from "./routes/job.routes.js";
import mapRouter from "./routes/map.routes.js";
import notificationRouter from "./routes/notification.routes.js";
import truckRouter from "./routes/trucks.routes.js";
import bodyParser from "body-parser";

import { increaseRadiusOfHospital, updateJobStatus } from "./functions/job.js";

async function startServer() {
  const forms = multer();
  const app = express();

  // await connectMongo();


  app.use(cors());
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  app.use((req, res, next) => {
    next();
  });

  app.use(addressRouter);
  app.use(emergencyCaseRouter);
  app.use(emergencyContactRouter);
  app.use(hospitalRouter);
  app.use(insuranceRouter);
  app.use(medicalInformationRouter);
  app.use(medicalStaffRouter);
  app.use(pharmacistRouter);
  app.use(pharmacyRouter);
  app.use(productRouter);
  app.use(orderRouter);
  app.use(userRouter);
  app.use(authRouter);
  app.use(chatRouter);
  app.use(messageRouter);
  app.use(userChatRouter);
  app.use(medicineRouter);
  app.use(notificationRouter);
  app.use(truckRouter);

  app.use("/job/create", async (req, res, next) => {
    const jobOptions = {
      removeOnComplete: true,
      delay: 20000,
      attempts: 3,
      jobId: "643d053be5c3dae4b695fc9e-1",
    };

    try {
      await connectQueue("job-queue").add(
        {
          type: "job",
          event: "create job",
        },
        jobOptions
      );
      res.status(200).send("job created");
    } catch (error) {
      res.status(500).send("job failed");
    }
  });

  // app.use("/job/cancel", async (req, res, next) => {
  //   const id = "jobId";
  //   const job = await connectQueue("auction-queue").getJob(id);
  //   await job?.remove();
  // });

  app.use(jobRouter);
  app.use(mapRouter);
  app.use(ErrorHandler);
  app.use("/", (req, res, next) => {
    res.status(404).send("Welcome to the Healthcare API (development) ");
  });

  app
    .listen(config.port, () => {
      console.log(`🛡️  Server listening on port: ${config.port} 🛡️`);
    })
    .on("error", (err) => {
      process.exit(1);
    });
}

startServer();
const jobQueue = connectQueue("job-queue");
const processJob = (job, done) => {
  try {
    done(null, "succes");
  } catch (error) {
    done(null, error);
  }
};
const initJob = () => {
  jobQueue.process(processJob);
  jobQueue.on("failed", (job) => {
    console.log("failed");
    console.log(job);
  });
  jobQueue.on("completed", async (job) => {
    const jobArray = job.id.split("-");
    let count = jobArray[1];
    const jobId = jobArray[0];

    if (count < 3) {
      console.log(count);
      const jobData = await increaseRadiusOfHospital({ jobId });

      const round = parseInt(count) + 1;
      console;
      const jobOptions = {
        removeOnComplete: true,
        delay: 20000,
        attempts: 3,
        jobId: jobId + "-" + round,
      };

      await connectQueue("job-queue").add(
        {
          type: "job",
          event: "create job",
        },
        jobOptions
      );

      console.log(jobData);

      job.remove();
    } else if (count == 3) {
      const jobData = await updateJobStatus({ jobId, status: "failed" });
      console.log(jobData);
    }
  });
  jobQueue.on("stalled", (job) => {
    console.log("stalled");
    console.log(job);
  });
};

initJob();
