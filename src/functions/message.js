import mongoose from "mongoose";
import { db } from "../loaders/firebase.js";
import { updateLastMessage } from "./group.js";

export async function sendMessage(payload) {
  const { groupId, message, sendBy, seen, type } = payload;
  const sendAt = new Date();

  const messageData = {
    message,
    sendBy,
    sendAt,
    seen,
    type,
  };

  //update last message
  await updateLastMessage(groupId, message, sendBy, sendAt);

  const messagesRef = db
    .collection("messages")
    .doc(groupId)
    .collection("messages");

  messagesRef
    .add(messageData)
    .then((docRef) => {
      console.log("Document written with ID: ", docRef.id);
    })
    .catch((error) => {
      console.error("Error adding document: ", error);
    });
}

export async function readCollection() {
  db.collection("messages")
    .get()
    .then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data()}`);
      });
    });
}
