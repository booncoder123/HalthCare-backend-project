import JobModel from "../models/job.js";
import { db } from "../loaders/firebase.js";
import UserModel from "../models/user.js";
import Notification from "../models/notification.model.js";
import NotificationToken from "../models/notificationToken.model.js";
import mongoose from "mongoose";
import { returnUserById, returnUserByUid } from "./user.js";
import {
  returnAddressById,
  returnAddressByUserId,
  returnHospitalAddress,
  returnPharmacyAddress,
} from "./address.js";

import {
  returnPharmacyById,
  returnPharmacyByPharmacistId,
} from "./pharmacy.js";
import connectQueue from "../loaders/queue.js";
import { createGroupCollection } from "./group.js";

import {
  returnPharmacistById,
  returnPharmacistByUserId,
} from "./pharmacist.js";
import {
  returnMedicalInformationById,
  updateMedicalInformationById,
} from "./medicalInformation.js";
import { updateMedicalStaffById } from "./medicalStaff.js";
import { returnHospitalById } from "./hospital.js";
import fetch from "node-fetch";

const isMongooseId = mongoose.Types.ObjectId.isValid;

export async function createJob(type, uid, n) {
  const job = await JobModel.create({
    requesterUid: uid,
    type,
  });
  const user = await returnUserByUid(uid);

  await serverSendNotificationToUser(
    "Request is successfully created",
    "Your request has been created successfully. Please wait a moment.",
    uid
  );

  const addressUser = await returnAddressByUserId(user.user._id);
  const cityUser = addressUser.city;

  //get lat long from firestore
  const userLocation = await getLocationByUid(uid);
  const userLatitude = userLocation.emergencyCase.coordinate.latitude;
  const userLongitude = userLocation.emergencyCase.coordinate.longitude;

  if (type == "emergencyCase") {
    const hospitals = await returnHospitalAddress(cityUser);

    console.log(hospitals);

    // sort hospitals by distance from user location
    const distances = await Promise.all(
      hospitals.map(async (hospital) => {
        const distance = await calculateDistance(
          parseFloat(userLatitude),
          parseFloat(userLongitude),
          parseFloat(hospital.address.latitude),
          parseFloat(hospital.address.longitude),
          "K"
        );
        return { hospital, distance };
      })
    );
    distances.sort((a, b) => a.distance - b.distance);

    const numOfElement = Math.ceil(distances.length / n);

    const users = distances.slice(0, numOfElement);

    await createJobCollection(
      "finding",
      job._id.toString(),
      users,
      "1",
      distances,
      "emergencyCase"
    );

    return job;
  } else if (type == "pharmacy") {
    const pharmacies = await returnPharmacyAddress(cityUser);

    // sort hospitals by distance from user location
    const distances = await Promise.all(
      pharmacies.map(async (pharmacy) => {
        const distance = await calculateDistance(
          parseFloat(userLatitude),
          parseFloat(userLongitude),
          parseFloat(pharmacy.address.latitude),
          parseFloat(pharmacy.address.longitude),
          "K"
        );
        return { pharmacy, distance };
      })
    );
    distances.sort((a, b) => a.distance - b.distance);

    const numOfElement = Math.ceil(distances.length / n);

    let pharmaciesInrange = distances.slice(0, numOfElement);

    for (let i = 0; i < pharmaciesInrange.length; i++) {
      const pharmacy = pharmaciesInrange[i];
      const pharmacistId = pharmacy.pharmacy.pharmacistId;

      const pharmacist = await returnPharmacistById(
        mongoose.Types.ObjectId(pharmacistId)
      );
      if (pharmacist != null) {
        const userId = pharmacist.userId;
        const user = await returnUserById(userId);
        const uid = user.user.uid;
      }
    }

    await createJobCollection(
      "finding",
      job._id.toString(),
      pharmaciesInrange,
      "1",
      distances,
      "pharmacy"
    );

    const jobOptions = {
      removeOnComplete: true,
      delay: 20000,
      attempts: 3,
      jobId: job._id + "-1",
    };

    await connectQueue("job-queue").add(
      {
        type: "job",
        event: "create job",
      },
      jobOptions
    );

    return job;
  }
}

async function serverSendNotificationToUser(title, body, receiver) {
  try {
    const mapsRef = db.collection("notification").doc(receiver);
    const doc = await mapsRef.get();

    const user = await UserModel.findOne({
      uid: receiver,
    });

    const { _id } = user;

    const notification = new Notification({
      userId: _id,
      title,
      body,
      // feature,
      // groupId,
      // type,
    });
    await notification.save();

    const expoNotificationBody = {
      to: doc.data().tokens,
      title,
      body,
      // data: {
      //   groupId,
      //   feature,
      // },
      sound: "default",
      ios: {
        // optional configuration for iOS
        sound: true, // enable sound on iOS
      },
      android: {
        // optional configuration for Android
        channelId: "default",
        sound: true, // enable sound on Android
        priority: "max",
        group: "pecgo",
      },
    };

    const response = await fetch("https://exp.host/--/api/v2/push/send", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "accept-encoding": "gzip, deflate",
        host: "exp.host",
        "accept-language": "en-US,en;q=0.9",
      },
      body: JSON.stringify(expoNotificationBody),
    });

    return response;
  } catch (error) {
    console.log({ error });
    if (!error.statusCode) {
      error.statusCode = 500;
    }
  }
}

export async function calculateDistance(lat1, lon1, lat2, lon2, unit) {
  if (lat1 == lat2 && lon1 == lon2) {
    return 0;
  } else {
    var radlat1 = (Math.PI * lat1) / 180;
    var radlat2 = (Math.PI * lat2) / 180;
    var theta = lon1 - lon2;
    var radtheta = (Math.PI * theta) / 180;
    var dist =
      Math.sin(radlat1) * Math.sin(radlat2) +
      Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
      dist = 1;
    }
    dist = Math.acos(dist);
    dist = (dist * 180) / Math.PI;
    dist = dist * 60 * 1.1515;
    if (unit == "K") {
      dist = dist * 1.609344;
    }
    if (unit == "N") {
      dist = dist * 0.8684;
    }
    return dist;
  }
}

export async function getLocationByUid(uid) {
  const docRef = db.collection("maps").doc(uid);
  const doc = await docRef.get();

  if (doc.exists) {
    const data = doc.data();
    return data;
  } else {
    console.log(`No document found with id: ${uid}`);
    return null;
  }
}

export async function returnAllJobByPharmacistTok(uid) {
  try {
    const user = await returnUserByUid(uid);
    const userId = user.user._id;
    const pharmacist = await returnPharmacistByUserId(userId);
    const pharmacy = await returnPharmacyByPharmacistId(pharmacist._id);

    const pharmacy0 = pharmacy[0];
    const pharmacyId = pharmacy0._id;

    const querySnapshot = await db.collection("jobs").get();
    const jobDataArray = [];
    for (const doc of querySnapshot.docs) {
      const jobData = doc.data();
      const users = jobData.users || [];
      const matchingUsers = users.filter(
        (userId) =>
          userId === pharmacyId.toString() || userId === pharmacyId.toString()
      );

      if (matchingUsers.length > 0) {
        const job = await returnJobByJobId(jobData.jobId);
        jobDataArray.push({
          job,
          jobData,
        });
      }
    }

    for (let job of jobDataArray) {
      let requesterUser = await returnUserByUid(job.job.requesterUid);
      requesterUser = JSON.parse(JSON.stringify(requesterUser));

      let requesterMedicalInformation = await returnMedicalInformationById(
        requesterUser.user.medicalId
      );
      requesterMedicalInformation = JSON.parse(
        JSON.stringify(requesterMedicalInformation)
      );

      let requesterAddress = await returnAddressById(
        requesterUser.user.addressId
      );
      requesterAddress = JSON.parse(JSON.stringify(requesterAddress));
      job.requesterName = requesterMedicalInformation.name;
      job.requesterAddress = requesterAddress;
    }

    return jobDataArray;
  } catch (error) {
    console.log(`Error getting documents: ${error}`);
  }
}

export async function returnJobRequesterById(id) {
  let job = await returnJobByJobId(id);
  job = JSON.parse(JSON.stringify(job));

  let requesterUser = await returnUserByUid(job.requesterUid);
  requesterUser = JSON.parse(JSON.stringify(requesterUser));

  job.requesterUser = requesterUser;

  return job;
}

export async function returnJobReceiverById(id) {
  let job = await returnJobByJobId(id);
  job = JSON.parse(JSON.stringify(job));

  let requesterUser = await returnUserByUid(job.requesterUid);
  let receiverUser = await returnUserByUid(job.receiverUid);

  requesterUser = JSON.parse(JSON.stringify(requesterUser));
  receiverUser = JSON.parse(JSON.stringify(receiverUser));

  const hospitalId = receiverUser.paramedics.hospitalId;
  const hospital = await returnHospitalById(hospitalId);
  const hospitalAddress = await returnAddressById(hospital.addressId);

  job.requesterUser = requesterUser;
  job.receiverUser = receiverUser;
  job.receiverUser.hospital = hospital;
  job.receiverUser.hospitalAddress = hospitalAddress;

  return job;
}

export async function returnPharmacistByJobId(id) {
  let job = await returnJobByJobId(id);
  job = JSON.parse(JSON.stringify(job));

  const pharmacistUid = job.receiverUid;

  let pharmacistProfile = await returnUserByUid(pharmacistUid);
  pharmacistProfile = JSON.parse(JSON.stringify(pharmacistProfile));
  job.pharmacistProfile = pharmacistProfile;

  return job;
}

export async function createJobCollection(
  status,
  jobId,
  users,
  round,
  distances,
  type
) {
  const job = await returnJobByJobId(jobId);
  console.log(job);
  if (type == "emergencyCase") {
    const jobData = {
      status,
      jobId,
      created_at: job.created_at,
      users: users.map((user) => user.hospital._id.toString()),
      round,
      allusers: distances.map(
        (user) => user.hospital._id.toString() // convert ObjectId to string
      ),
    };

    const jobsRef = db.collection("jobs").doc(jobId);

    jobsRef
      .set(jobData)
      .then(() => {
        console.log("Document created with id: ", jobId);
      })
      .catch((error) => {
        console.error("Error adding document: ", error);
      });
  } else if (type == "pharmacy") {
    const jobData = {
      status,
      jobId,
      created_at: job.created_at,
      users: users.map(
        (user) => user.pharmacy._id.toString() // convert ObjectId to string
      ),
      round,
      allusers: distances.map(
        (user) => user.pharmacy._id.toString() // convert ObjectId to string
      ),
    };

    const jobsRef = db.collection("jobs").doc(jobId);

    jobsRef
      .set(jobData)
      .then(() => {
        console.log("Document created with id: ", jobId);
      })
      .catch((error) => {
        console.error("Error adding document: ", error);
      });
  }
}

export async function returnAllJob() {
  return await JobModel.find({
    isDeleted: false,
  });
}

export async function cancelJobByJobId(payload) {
  try {
    const { jobId, round } = payload;
    const id = jobId + "-" + round;
    const job = await connectQueue("job-queue").getJob(id);
    await job?.remove();

    const jobStatus = updateJobStatus({ jobId, status: "failed" });

    return jobStatus;
  } catch (error) {
    throw {
      message: error.message,
      status: error.status,
    };
  }
}

export async function acceptJobByJobId(payload, uid) {
  const user = await returnUserByUid(uid);

  const { jobId, receiverUid } = payload;

  const jobsRef = db.collection("jobs");
  const querySnapshot = await jobsRef
    .where("jobId", "==", jobId.toString())
    .get();

  const jobData = [];
  querySnapshot.forEach((doc) => {
    jobData.push({ id: doc.id, ...doc.data() });
  });

  const round = jobData[0].round;

  const id = jobId + "-" + round;
  const job = await connectQueue("job-queue").getJob(id);
  await job?.remove();

  const jobStatus = await updateJobStatus({
    jobId,
    status: "doing",
    receiverUid: receiverUid ? receiverUid.value : user.user.uid,
  });

  const receiverProfile = await returnUserByUid(
    receiverUid ? receiverUid.value : uid
  );

  //update paramedic to onDuty
  if (receiverProfile.user.role == "paramedics") {
    await updateMedicalStaffById(
      { isOnDuty: false },
      receiverProfile.paramedics.id
    );
  }

  await createGroupCollection({
    uid: [
      jobStatus.job.requesterUid.toString(),
      jobStatus.job.receiverUid.toString(),
    ],
    jobId,
  });

  return { jobStatus };
}

export async function doneJobByJobId(payload) {
  const { jobId } = payload;

  const jobStatus = await updateJobStatus({
    jobId,
    status: "done",
  });

  //update status paramedic to available
  const job = await returnJobByJobId(jobId);
  const receiverProfile = await returnUserByUid(job.receiverUid);
  if (receiverProfile.user.role == "paramedics") {
    await updateMedicalStaffById(
      { isOnDuty: false },
      receiverProfile.paramedics.id
    );
  }

  const groupsRef = db.collection("groups");
  const querySnapshot = await groupsRef.where("jobId", "==", jobId).get();
  if (!querySnapshot.empty) {
    const docRef = querySnapshot.docs[0].ref;
    await docRef.update({
      visible: false,
    });
    const updatedSnapshot = await docRef.get();
    const updatedData = updatedSnapshot.data();
    return { updatedData, jobStatus };
  }
}

export async function increaseRadiusOfHospital(payload) {
  const { jobId } = payload;
  const n = 3;
  const jobsRef = db.collection("jobs");
  const querySnapshot = await jobsRef.where("jobId", "==", jobId).get();

  const jobData = [];
  querySnapshot.forEach((doc) => {
    jobData.push({ id: doc.id, ...doc.data() });
  });

  const numOfElement =
    (jobData[0].allusers.length / n) * (parseInt(jobData[0].round) + 1);

  jobData[0].users = jobData[0].allusers.slice(0, numOfElement);
  jobData[0].round = parseInt(jobData[0].round) + 1;

  // Update Firestore fields
  const docRef = db.collection("jobs").doc(jobId);
  await docRef.update({
    users: jobData[0].users,
    round: jobData[0].round,
  });

  //loop for noti
  for (let i = 0; i < jobData[0].users.length; i++) {
    const pharmacyId = jobData[0].users[i];

    const pharmacy = await returnPharmacyById(
      mongoose.Types.ObjectId(pharmacyId)
    );
    if (pharmacy) {
      const pharmacistId = pharmacy.pharmacistId;

      const pharmacist = await returnPharmacistById(
        mongoose.Types.ObjectId(pharmacistId)
      );

      if (pharmacist != null) {
        const userId = pharmacist.userId;
        const user = await returnUserById(userId);
        const uid = user.user.uid;

        if (uid) {
          serverSendNotificationToUser(
            "Expanding area",
            "We are expanding the search area, please wait a moment.",
            uid
          );
        }
      }
    }
  }

  return jobData;
}

export async function updateJobStatus(payload) {
  const { jobId, status, receiverHospitalId, receiverPharmacyId, receiverUid } =
    payload;

  let pharmacy;
  let pharmacist;

  if (receiverPharmacyId != null) {
    pharmacy = await returnPharmacyById(receiverPharmacyId);
    pharmacist = await returnPharmacistById(pharmacy.pharmacistId);
  }

  //update in mongo field
  const job = await JobModel.findOneAndUpdate(
    { _id: jobId },
    {
      status,
      receiver: receiverHospitalId ? receiverHospitalId : receiverPharmacyId,
      receiverUid,
    },
    {
      new: true,
      omitUndefined: true,
    }
  );

  const jobsRef = db.collection("jobs");
  const querySnapshot = await jobsRef
    .where("jobId", "==", jobId.toString())
    .get();

  const jobData = [];
  querySnapshot.forEach((doc) => {
    jobData.push({ id: doc.id, ...doc.data() });
  });

  jobData[0].status = status;

  // Update Firestore fields
  const docRef = db.collection("jobs").doc(jobId.toString());
  await docRef.update({
    status: status,
  });

  //send notification
  if (receiverUid) {
    await serverSendNotificationToUser(
      `job is ${status}`,
      `job is ${status} Please check the app to see further information`,
      job.receiverUid
    );
  }

  await serverSendNotificationToUser(
    `job is ${status}`,
    `job is ${status} Please check the app to see further information`,
    job.requesterUid
  );

  return { jobData, job, pharmacist };
}

export async function returnJobByJobId(id) {
  const job = await JobModel.findOne({
    _id: id,
  });

  if (!job) {
    throw {
      message: `job id ${id} not found`,
      status: 404,
    };
  }

  return job;
}
