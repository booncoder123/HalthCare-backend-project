import AddressModel from "../models/address.js";
import UserModel from "../models/user.js";
import HospitalModel from "../models/hospital.js";
import PharmacyModel from "../models/pharmacy.js";

import mongoose from "mongoose";

const isMongooseId = mongoose.Types.ObjectId.isValid;

export async function createAddress(payload) {
  const {
    userId,
    hospitalId,
    pharmacyId,
    address,
    city,
    zipCode,
    latitude,
    longitude,
    type,
  } = payload;

  const createdAddress = await AddressModel.create({
    address,
    city,
    zipCode,
    latitude,
    longitude,
    type,
  });

  if (isMongooseId(userId)) {
    const user = await UserModel.findOneAndUpdate(
      {
        _id: userId,
      },
      {
        addressId: createdAddress._id,
      },
      { new: true, omitUndefined: true }
    );
  } else if (isMongooseId(hospitalId)) {
    await HospitalModel.findOneAndUpdate(
      {
        _id: hospitalId,
        isDeleted: false,
      },
      {
        addressId: createdAddress._id,
      },
      { new: true, omitUndefined: true }
    );
  } else if (isMongooseId(pharmacyId)) {
    await PharmacyModel.findOneAndUpdate(
      {
        _id: pharmacyId,
        isDeleted: false,
      },
      {
        addressId: createdAddress._id,
      },
      { new: true, omitUndefined: true }
    );
  } else {
    throw {
      message:
        "please specify id(hospital, user, pharmacy) in which the address belongs to",
      status: 404,
    };
  }

  return createdAddress;
}

export async function returnAllAddress() {
  return await AddressModel.find({
    isDeleted: false,
  });
}

export async function returnAddressById(id) {
  if (isMongooseId(id)) {
    return await AddressModel.findOne({
      _id: id,
      isDeleted: false,
    });
  } else {
    throw {
      message: "id is not valid",
      status: 404,
    };
  }
}

export async function returnAddressByUserId(userId) {
  let user;
  if (isMongooseId(userId)) {
    user = await UserModel.findOne({
      _id: userId,
      isDeleted: false,
    });
  } else {
    throw {
      message: "user id not found",
      status: 404,
    };
  }
  const addressId = user.addressId;
  return await AddressModel.findOne({
    _id: addressId,
    isDeleted: false,
  });
}

export async function returnHospitalAddress(city) {
  const hospitals = await HospitalModel.aggregate([
    {
      $lookup: {
        from: "addresses",
        localField: "addressId",
        foreignField: "_id",
        as: "address",
      },
    },
    {
      $unwind: {
        path: "$address",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $match: {
        "address.city": city,
      },
    },
  ]);

  return hospitals;
}

export async function returnPharmacyAddress(city) {
  const pharmacies = await PharmacyModel.aggregate([
    {
      $lookup: {
        from: "addresses",
        localField: "addressId",
        foreignField: "_id",
        as: "address",
      },
    },
    {
      $unwind: {
        path: "$address",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $match: {
        "address.city": city,
      },
    },
  ]);

  return pharmacies;
}

export async function returnAllPharmacyAddresses() {
  const pharmacies = await PharmacyModel.aggregate([
    {
      $lookup: {
        from: "addresses",
        localField: "addressId",
        foreignField: "_id",
        as: "address",
      },
    },
    {
      $unwind: {
        path: "$address",
        preserveNullAndEmptyArrays: true,
      },
    },
  ]);

  return pharmacies;
}

export async function updateAddressById(payload, id) {
  const {
    hospitalId,
    pharmacyId,
    address,
    city,
    zipCode,
    latitude,
    longitude,
    type,
  } = payload;
  if (isMongooseId(id)) {
    return await AddressModel.findOneAndUpdate(
      {
        _id: id,
        isDeleted: false,
      },
      {
        hospitalId,
        pharmacyId,
        address,
        city,
        zipCode,
        latitude,
        longitude,
        type,
      },
      { new: true, omitUndefined: true }
    );
  } else {
    throw {
      message: "id is not valid",
      status: 404,
    };
  }
}

export async function softDeleteAddressById(id) {
  if (isMongooseId(id)) {
    return await AddressModel.findOneAndUpdate(
      {
        _id: id,
        isDeleted: false,
      },
      {
        isDeleted: true,
      },
      { new: true, omitUndefined: true }
    );
  } else {
    throw {
      message: "id is not valid",
      status: 404,
    };
  }
}
