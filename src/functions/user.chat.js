import mongoose from "mongoose";
import { db } from "../loaders/firebase.js";

export async function createUserCollection(payload) {
  const { uid } = payload;

  const userData = {
    uid,
  };

  const usersRef = db.collection("users");

  usersRef
    .doc(uid)
    .set(userData)
    .then(() => {
      console.log("Document successfully written");
    })
    .catch((error) => {
      console.error("Error writing document: ", error);
    });
}

export async function readCollection() {
  db.collection("users")
    .get()
    .then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data()}`);
      });
    });
}
