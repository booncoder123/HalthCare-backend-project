import mongoose from "mongoose";
import { db } from "../loaders/firebase.js";
import { updateLastMessage } from "./group.js";

export async function createCoordByUid(uid, payload) {
  const { latitude, longitude } = payload;
  const now = new Date();

  const coordinateData = {
    latitude,
    longitude,
    now,
  };

  const trucksRef = db.collection("trucks").doc(uid).collection("coordinates");

  trucksRef
    .add(coordinateData)
    .then((docRef) => {
      console.log("Document written with ID: ", docRef.id);
    })
    .catch((error) => {
      console.error("Error adding document: ", error);
    });
}

