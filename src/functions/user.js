import UserModel from "../models/user.js";
import PharmacistModel from "../models/pharmacist.js";
import MedicalStaffModel from "../models/medicalStaff.js";
import MedicalInformationModel from "../models/medicalInformation.js";
import AddressModel from "../models/address.js";
import InsuranceModel from "../models/Insurance.js";
import HospitalModel from "../models/hospital.js";
import mongoose from "mongoose";
import { returnPharmacyByPharmacistId } from "./pharmacy.js";
import { returnHospitalById } from "./hospital.js";

const isMongooseId = mongoose.Types.ObjectId.isValid;

export async function createUser(payload, uid, faceImgPath) {
  const {
    addressId,
    contactId,
    medicalId,
    role,
    email,
    faceImg,
    nationalIdImg,
    verificationStatus,
    emailVerify,
    providerId,
  } = payload;

  //med info
  const {
    allergies,
    citizenId,
    phoneNumber,
    congenitalDisease,
    DNRStatus,
    gender,
    name,
    organDonour,
    dateOfBirth,
    bloodType,
    powerOfAttorneyName,
    powerOfAttorneyPhoneNumber,
    powerOfAttorneyRelationship,
    regularMed,
  } = payload;

  const medicalInformation = await MedicalInformationModel.create({
    allergies,
    citizenId,
    phoneNumber,
    congenitalDisease,
    DNRStatus,
    gender,
    name,
    organDonour,
    dateOfBirth,
    bloodType,
    powerOfAttorneyName,
    powerOfAttorneyPhoneNumber,
    powerOfAttorneyRelationship,
    regularMed,
  });

  //address
  const {
    hospitalId,
    pharmacyId,
    address,
    city,
    zipCode,
    latitude,
    longitude,
    type,
  } = payload;

  const createdAddress = await AddressModel.create({
    hospitalId,
    pharmacyId,
    address,
    city,
    zipCode,
    latitude,
    longitude,
    type,
  });

  let errorMessage = "";
  if (addressId && !isMongooseId(addressId))
    errorMessage += "address id is not mongoose id";
  if (contactId && !isMongooseId(contactId))
    errorMessage += "contact id is not mongoose id";
  if (medicalId && !isMongooseId(medicalId))
    errorMessage += "medical id is not mongoose id";

  if (errorMessage !== "") {
    throw {
      message: errorMessage,
      status: 404,
    };
  }

  let user;
  if (faceImgPath) {
    user = await UserModel.create({
      addressId: createdAddress._id,
      contactId,
      medicalId: medicalInformation._id,
      role,
      email,
      faceImg: faceImgPath[0],
      nationalIdImg,
      verificationStatus,
      emailVerify,
      providerId,
      uid,
    });
  } else {
    user = await UserModel.create({
      addressId: createdAddress._id,
      contactId,
      medicalId: medicalInformation._id,
      role,
      email,
      nationalIdImg,
      verificationStatus,
      emailVerify,
      providerId,
      uid,
    });
  }

  //insurance
  const { provider, insuranceNumber, plan, expirationDate } = payload;

  const insurance = await InsuranceModel.create({
    userId: user._id,
    insuranceNumber,
    provider,
    plan,
    expirationDate,
  });

  if (role == "pharmacist") {
    const {
      imgId,
      pharmacistVerificationStatus,
      licenseId,
      licenseExpireDate,
    } = payload;

    const pharmacist = await PharmacistModel.create({
      userId: user._id,
      imgId,
      verificationStatus: pharmacistVerificationStatus,
      licenseId,
      licenseExpireDate,
    });

    return await {
      user,
      medicalInformation,
      pharmacist,
      insurance,
      createdAddress,
    };
  } else if (role == "paramedics") {
    const { hospitalId, role, licenseId, licenseExpireDate } = payload;

    const paramedics = await MedicalStaffModel.create({
      userId: user._id,
      hospitalId,
      role,
      licenseId,
      licenseExpireDate,
    });

    return await {
      user,
      medicalInformation,
      paramedics,
      insurance,
      createdAddress,
    };
  }

  return await { user, medicalInformation, insurance, createdAddress };
}

export async function returnAllUser() {
  return await UserModel.find({
    isDeleted: false,
  });
}

export async function returnUserById(id) {
  if (isMongooseId(id)) {
    const user = await UserModel.findOne({
      _id: id,
      isDeleted: false,
    });
    if (!user) {
      throw {
        message: `user not found from user id: ${id}`,
        status: 404,
      };
    }
    const address = await AddressModel.findOne({
      _id: user.addressId,
    });

    if (!address) {
      throw {
        message: `address not found from user id: ${id}`,
        status: 404,
      };
    }

    const medicalInformation = await MedicalInformationModel.findOne({
      _id: user.medicalId,
    });

    if (!medicalInformation) {
      throw {
        message: `medical information not found from user id: ${id}`,
        status: 404,
      };
    }

    const insurance = await InsuranceModel.findOne({
      userId: user._id,
    });

    if (!insurance) {
      throw {
        message: `insurance not found from user id: ${id}`,
        status: 404,
      };
    }

    if (user.role == "paramedics") {
      const paramedics = await MedicalStaffModel.findOne({ userId: user._id });

      return {
        user,
        address,
        medicalInformation,
        insurance,
        paramedics,
      };
    } else if (user.role == "pharmacist") {
      const pharmacist = await PharmacistModel.findOne({ userId: user._id });

      return {
        user,
        address,
        medicalInformation,
        insurance,
        pharmacist,
      };
    } else {
      return { user, address, medicalInformation, insurance };
    }
  } else {
    throw {
      message: `userId ${id} is not mongoose id`,
      status: 404,
    };
  }
}

export async function returnUserByUid(uid) {
  const user = await UserModel.findOne({
    uid,
  });

  const address = await AddressModel.findOne({
    _id: user.addressId,
  });

  const medicalInformation = await MedicalInformationModel.findOne({
    _id: user.medicalId,
  });

  const insurance = await InsuranceModel.findOne({
    userId: user._id,
  });

  if (user.role == "paramedics") {
    const paramedics = await MedicalStaffModel.findOne({ userId: user._id });
    const hospital = await returnHospitalById(paramedics.hospitalId);

    return {
      user,
      address,
      medicalInformation,
      insurance,
      paramedics,
      hospital,
    };
  } else if (user.role == "pharmacist") {
    const pharmacist = await PharmacistModel.findOne({ userId: user._id });

    const pharmacy = (await returnPharmacyByPharmacistId(pharmacist._id))[0];

    return {
      user,
      address,
      medicalInformation,
      insurance,
      pharmacist,
      pharmacy,
    };
  } else if (user.role == "hospital") {
    const hospital = await HospitalModel.findOne({ _id: user.hospitalId });

    return {
      user,
      address,
      medicalInformation,
      insurance,
      hospital,
    };
  } else {
    return { user, address, medicalInformation, insurance, address };
  }
}

export async function updateUserById(payload, uid, attachedImagePath) {
  const {
    addressId,
    contactId,
    medicalId,
    role,
    email,
    faceImg,
    nationalIdImg,
    verificationStatus,
    emailVerify,
    providerId,
  } = payload;

  //med info
  const {
    allergies,
    citizenId,
    phoneNumber,
    congenitalDisease,
    DNRStatus,
    gender,
    name,
    organDonour,
    dateOfBirth,
    bloodType,
    powerOfAttorneyName,
    powerOfAttorneyPhoneNumber,
    powerOfAttorneyRelationship,
    regularMed,
  } = payload;

  //address
  const {
    hospitalId,
    pharmacyId,
    address,
    city,
    zipCode,
    latitude,
    longitude,
    type,
  } = payload;

  //insurance
  const { provider, insuranceNumber, plan, expirationDate } = payload;

  try {
    const user = await UserModel.findOneAndUpdate(
      {
        uid: uid,
        isDeleted: false,
      },

      {
        addressId,
        contactId,
        medicalId,
        role,
        email,
        faceImg: attachedImagePath,
        nationalIdImg,
        verificationStatus,
        emailVerify,
        providerId,
        uid,
      },
      {
        new: true,
        omitUndefined: true,
      }
    );

    const medicalInformation = await MedicalInformationModel.findOneAndUpdate(
      {
        _id: user.medicalId,
        isDeleted: false,
      },
      {
        allergies,
        citizenId,
        phoneNumber,
        congenitalDisease,
        DNRStatus,
        gender,
        name,
        organDonour,
        dateOfBirth,
        bloodType,
        powerOfAttorneyName,
        powerOfAttorneyPhoneNumber,
        powerOfAttorneyRelationship,
        regularMed,
      },
      {
        new: true,
        omitUndefined: true,
      }
    );

    const createdAddress = await AddressModel.findOneAndUpdate(
      {
        _id: user.addressId,
        isDeleted: false,
      },
      {
        hospitalId,
        pharmacyId,
        address,
        city,
        zipCode,
        latitude,
        longitude,
        type,
      },
      {
        new: true,
        omitUndefined: true,
      }
    );

    const insurance = await InsuranceModel.findOneAndUpdate(
      {
        userId: user._id,
        isDeleted: false,
      },
      {
        provider,
        plan,
        expirationDate,
        insuranceNumber,
      },
      {
        new: true,
        omitUndefined: true,
      }
    );

    if (user.type == "pharmacist") {
      const {
        imgId,
        pharmacistVerificationStatus,
        licenseId,
        licenseExpireDate,
      } = payload;

      const pharmacist = await PharmacistModel.findOneAndUpdate(
        {
          userId: user._id,
          isDeleted: false,
        },
        {
          imgId,
          pharmacistVerificationStatus,
          licenseId,
          licenseExpireDate,
        },
        {
          new: true,
          omitUndefined: true,
        }
      );

      return {
        user,
        medicalInformation,
        createdAddress,
        insurance,
        pharmacist,
      };
    } else if (user.type == "paramedics") {
      const { hospitalId, role, licenseId, licenseExpireDate } = payload;

      const paramedics = await MedicalStaffModel.findOneAndUpdate(
        {
          userId: user._id,
          isDeleted: false,
        },
        {
          hospitalId,
          role,
          licenseId,
          licenseExpireDate,
        },
        {
          new: true,
          omitUndefined: true,
        }
      );

      return {
        user,
        medicalInformation,
        createdAddress,
        insurance,
        paramedics,
      };
    }

    return { user, medicalInformation, createdAddress, insurance };
  } catch (error) {
    throw {
      message: error,
      status: 404,
    };
  }
}

export async function softDeleteUserById(id) {
  if (isMongooseId(id)) {
    return await UserModel.findOneAndUpdate(
      {
        _id: id,
        isDeleted: false,
      },
      {
        isDeleted: true,
      },
      { new: true, omitUndefined: true }
    );
  } else {
    throw {
      message: "id is not valid",
      status: 404,
    };
  }
}

export async function checkEmailExist(email) {
  const user = await UserModel.findOne({
    email: email,
  });

  if (user) {
    return true;
  } else {
    return false;
  }
}
