import EmergencyCaseModel from "../models/emergencyCase.js";
import JobModel from "../models/job.js";
import MedicalStaffModel from "../models/medicalStaff.js";
import HospitalModel from "../models/hospital.js";
import UserModel from "../models/user.js";
import MedicalInformationModel from "../models/medicalInformation.js";
import { db } from "../loaders/firebase.js";
import { createJob, returnJobByJobId, updateJobStatus } from "./job.js";
import connectQueue from "../loaders/queue.js";
import { calculateDistance } from "../functions/job.js";

import mongoose from "mongoose";
import medicalStaff from "../models/medicalStaff.js";
import emergencyCase from "../models/emergencyCase.js";
import {
  returnDoneOrderByTokPharmacistId,
  returnDoneOrderByTokUserId,
  returnOrderByTokPharmacistId,
  returnOrderByTokUserId,
} from "./order.js";
import { returnUserById, returnUserByUid } from "./user.js";
import { returnAddressById } from "./address.js";

const isMongooseId = mongoose.Types.ObjectId.isValid;

export async function createEmergencyCase(payload, uid, attachedImagesPath) {
  const user = await UserModel.findOne({ uid });

  const {
    contactNumber,
    attachedImages,
    symptoms,
    otherInformation,
    alertness,
    alertnessLevel,
    patientNumber,
    urgencyLevel,
    acceptaceStatus,
    deliveringStatus,
    dateAndTimeConfirmArrival,
    latitude,
    longitude,
    assigneeName,
    assigneeNumber,
  } = payload;

  //create maps of emergency case in firebase
  await createMapCollection(uid, latitude, longitude);

  //create new job
  const job = await createJob("emergencyCase", uid, 3);

  const jobOptions = {
    removeOnComplete: true,
    delay: 20000,
    attempts: 3,
    jobId: job._id + "-1",
  };

  await connectQueue("job-queue").add(
    {
      type: "job",
      event: "create job",
    },
    jobOptions
  );

  return await EmergencyCaseModel.create({
    userId: user._id,
    contactNumber,
    attachedImages: attachedImagesPath,
    symptoms,
    otherInformation,
    alertness,
    alertnessLevel,
    patientNumber,
    urgencyLevel,
    acceptaceStatus,
    deliveringStatus,
    dateAndTimeConfirmArrival,
    assigneeName,
    assigneeNumber,
    jobId: job._id,
  });
}

export async function createMapCollection(uid, latitude, longitude, city) {
  const mapData = {
    emergencyCase: {
      coordinate: {
        latitude,
        longitude,
      },
    },
  };

  const mapsRef = db.collection("maps").doc(uid);

  mapsRef
    .set(mapData)
    .then(() => {
      console.log("Document written with ID: ", uid);
    })
    .catch((error) => {
      console.error("Error adding document: ", error);
    });
}

export async function returnEmergencyCaseById(id) {
  if (isMongooseId(id)) {
    return await EmergencyCaseModel.findOne({
      _id: id,
      isDeleted: false,
    });
  } else {
    throw {
      message: "id is invalid",
      status: 404,
    };
  }
}

export async function returnEmergencyCaseAndJobById(id) {
  const emergencyCase = await returnEmergencyCaseById(id);
  const job = await returnJobByJobId(emergencyCase.jobId);

  const jobRef = await db.collection("jobs").doc(job._id.toString());
  const jobDoc = await jobRef.get();
  if (!jobDoc.exists) {
    throw {
      message: `No such job document with ID '${jobId}'`,
      status: 404,
    };
  }
  const jobData = jobDoc.data();

  return { emergencyCase, job, jobData };
}

export async function returnEmergencyCaseByUserId(userId) {
  if (isMongooseId(userId)) {
    return await EmergencyCaseModel.findOne({
      userId: userId,
      isDeleted: false,
    });
  } else {
    throw {
      message: "id is invalid",
      status: 404,
    };
  }
}

export async function returnEmergencyCaseByUid(uid) {
  const user = await UserModel.findOne({ uid, isDeleted: false });
  if (!user) {
    throw {
      message: `user uid: ${uid} not found`,
      status: 404,
    };
  }

  let emergencyCases = await EmergencyCaseModel.find({
    userId: user._id,
    isDeleted: false,
  });
  if (!emergencyCases) {
    throw {
      message: `emergency case not found from user id ${user._id}`,
      status: 404,
    };
  }

  emergencyCases = JSON.parse(JSON.stringify(emergencyCases));

  emergencyCases = await addNameAndHospitalName(emergencyCases);

  return { emergencyCases };
}

export async function returnDoneEmergencyCaseByUid(uid) {
  const user = await UserModel.findOne({ uid, isDeleted: false });

  if (!user) {
    throw {
      message: `user uid: ${uid} not found`,
      status: 404,
    };
  }

  let emergencyCases = await EmergencyCaseModel.aggregate([
    {
      $match: {
        userId: user._id,
        isDeleted: false,
      },
    },

    {
      $lookup: {
        from: "jobs",
        localField: "jobId",
        foreignField: "_id",
        as: "job",
      },
    },
    {
      $unwind: {
        path: "$job",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $match: {
        "job.status": "done",
      },
    },
  ]);

  if (!emergencyCases) {
    throw {
      message: `emergency case not found from user id ${user._id}`,
      status: 404,
    };
  }

  emergencyCases = JSON.parse(JSON.stringify(emergencyCases));

  emergencyCases = await addNameAndHospitalName(emergencyCases);

  return { emergencyCases };
}

async function addNameAndHospitalName(emergencyCases) {
  for (const emergencyCase of emergencyCases) {
    let job = await JobModel.findOne({ _id: emergencyCase.jobId });

    if (job) {
      job = JSON.parse(JSON.stringify(job));
      emergencyCase.job = job;
    }

    if (job.status != "failed" && job.status != "finding") {
      //add receiver name
      const receiverUser = await UserModel.findOne({ uid: job.receiverUid });
      const receiverMedicalInformation = await MedicalInformationModel.findOne({
        _id: receiverUser.medicalId,
      });

      job.receiverName = receiverMedicalInformation.name;
      emergencyCase["job"] = job;

      //add receiver hospital name
      const paramedic = await MedicalStaffModel.findOne({
        userId: receiverUser._id,
      });

      if (!paramedic) {
        throw {
          message: `receiver of jobId: ${job._id} is not a paramedic`,
          status: 404,
        };
      }

      if (!paramedic.hospitalId) {
        throw {
          message: `paramedic _id ${paramedic._id} has no hospitalId`,
          status: 404,
        };
      }
      const hospital = await HospitalModel.findOne({
        _id: paramedic.hospitalId,
      });
      if (!hospital) {
        throw {
          message: `no hospital found from paramedic _id ${paramedic._id}`,
          status: 404,
        };
      }

      const hospitalAddress = JSON.parse(
        JSON.stringify(await returnAddressById(hospital.addressId))
      );

      const hospitalName = hospital.name;
      emergencyCase["hospitalName"] = hospitalName;
      emergencyCase["hospitalAddress"] = hospitalAddress;
    }
  }

  return emergencyCases;
}

export async function returnAllEmergencyCase() {
  return await EmergencyCaseModel.find({
    isDeleted: false,
  });
}

export async function returnEmergencyCaseByJobId(id) {
  const emergency = await EmergencyCaseModel.findOne({
    jobId: new mongoose.Types.ObjectId(id),
  });

  return emergency;
}

export async function returnEmergencyCaseByJobIdAndTok(uid, id) {
  const formatEmergencyCase = {};
  let emergencyCase = await returnEmergencyCaseByJobId(id);

  emergencyCase = JSON.parse(JSON.stringify(emergencyCase));

  const emergencyCaseUser = await returnUserById(emergencyCase.userId);
  const emergencyCaseUid = emergencyCaseUser.user.uid;

  const hospital = await returnUserByUid(uid);

  const hospitalLoingitude = hospital.address.longitude;
  const hospitalLatitude = hospital.address.latitude;
  let emergencyCaseLatitude;
  let emergencyCaseLongitude;

  const mapsRef = db.collection("maps");
  const docRef = mapsRef.doc(emergencyCaseUid);
  const docSnapshot = await docRef.get();

  if (docSnapshot.exists) {
    const emergencyCaseData = docSnapshot.data().emergencyCase;
    const coordinate = emergencyCaseData.coordinate;
    emergencyCaseLatitude = coordinate.latitude;
    emergencyCaseLongitude = coordinate.longitude;

    const distance = await calculateDistance(
      parseFloat(emergencyCaseLatitude),
      parseFloat(emergencyCaseLongitude),
      parseFloat(hospitalLatitude),
      parseFloat(hospitalLoingitude),
      "K"
    );

    formatEmergencyCase.distance = distance.toFixed(2);
  }

  const job = await returnJobByJobId(emergencyCase.jobId);

  const receiverProfile = job.receiverUid
    ? await returnUserByUid(job.receiverUid)
    : null;

  formatEmergencyCase.emergencyCaseId = emergencyCase._id;
  formatEmergencyCase.name = emergencyCaseUser.medicalInformation.name;
  formatEmergencyCase.symptoms = emergencyCase.symptoms;
  formatEmergencyCase.emergencyCaseLatitude = emergencyCaseLatitude;
  formatEmergencyCase.emergencyCaseLongitude = emergencyCaseLongitude;
  formatEmergencyCase.hospitalLatitude = hospitalLatitude;
  formatEmergencyCase.hospitalLoingitude = hospitalLoingitude;
  formatEmergencyCase.receiverProfile = receiverProfile;
  formatEmergencyCase.job = job;

  return formatEmergencyCase;
}

export async function returnEmergencyCaseByHospitalId(hospitalId) {
  try {
    const querySnapshot = await db.collection("jobs").get();
    const jobDataArray = [];
    for (const doc of querySnapshot.docs) {
      const jobData = doc.data();
      const users = jobData.users || [];
      const matchingUsers = users.filter(
        (userId) =>
          userId === hospitalId.toString() || userId === hospitalId.toString()
      );
      if (matchingUsers.length > 0) {
        const job = await returnJobByJobId(jobData.jobId);
        const emergencyCase = (await returnEmergencyCaseByJobId(job._id))[0];

        if (emergencyCase) {
          const userId = emergencyCase.userId;
          const userUid = (await returnUserById(userId)).user.uid;

          await jobDataArray.push({
            id: doc.id,
            users: jobData.users,
            jobId: jobData.jobId,
            job,
            emergencyCase,
            round: jobData.round,
            status: jobData.status,
            userUid,
          });
        } else {
          await jobDataArray.push({
            id: doc.id,
            users: jobData.users,
            jobId: jobData.jobId,
            job,
            emergencyCase,
            round: jobData.round,
            status: jobData.status,
          });
        }
      }
    }
    return jobDataArray;
  } catch (error) {
    console.log(`Error getting documents: ${error}`);
  }
}

export async function returnEmergencyCaseAndMedInfoById(uid, id) {
  const emergencyCase = await returnEmergencyCaseById(id);

  const formattedEmergencyCase = await returnEmergencyCaseByJobIdAndTok(
    uid,
    emergencyCase.jobId
  );
  const userProfile = await returnUserById(emergencyCase.userId);

  return { formattedEmergencyCase, userProfile, emergencyCase };
}

export async function returnEmergencyCaseAndOrderByTokUserId(uid) {
  const user = await returnUserByUid(uid);
  const role = await user.user.role;

  const emergencyCases = await returnDoneEmergencyCaseByUid(uid);

  const orders =
    role == "user" || role == "paramedics"
      ? await returnDoneOrderByTokUserId(uid)
      : role == "pharmacist"
      ? await returnDoneOrderByTokPharmacistId(uid)
      : "";

  return { emergencyCases, orders };
}

export async function updateEmergencyCaseStatusById(payload, id) {
  const { receiverHospitalId, status } = payload;

  const emergencyCase = await returnEmergencyCaseById(id);

  const { jobData, job } = await updateJobStatus({
    jobId: emergencyCase.jobId,
    status,
    receiverHospitalId,
  });

  return { jobData, job, emergencyCase };
}

export async function updateEmergencyCaseById(payload, id) {
  const {
    contactNumber,
    attachedImages,
    symptoms,
    otherInformation,
    alertness,
    alertnessLevel,
    patientNumber,
    urgencyLevel,
    acceptaceStatus,
    deliveringStatus,
    dateAndTimeConfirmArrival,
    latitude,
    longitude,
    assigneeName,
    assigneeNumber,
  } = payload;

  if (isMongooseId(id)) {
    return await EmergencyCaseModel.findOneAndUpdate(
      {
        _id: id,
        isDeleted: false,
      },
      {
        contactNumber,
        attachedImages,
        symptoms,
        otherInformation,
        alertness,
        alertnessLevel,
        patientNumber,
        urgencyLevel,
        acceptaceStatus,
        deliveringStatus,
        dateAndTimeConfirmArrival,
        latitude,
        longitude,
        assigneeName,
        assigneeNumber,
      },
      { new: true, omitUndefined: true }
    );
  } else {
    throw {
      message: "id is invalid",
      status: 404,
    };
  }
}

export async function updateEmergencyCaseByUid(payload, uid) {
  const user = await UserModel.findOne({ uid });
  const {
    contactNumber,
    attachedImages,
    symptoms,
    otherInformation,
    alertness,
    alertnessLevel,
    patientNumber,
    urgencyLevel,
    acceptaceStatus,
    deliveringStatus,
    dateAndTimeConfirmArrival,
    latitude,
    longitude,
    assigneeName,
    assigneeNumber,
  } = payload;

  return await EmergencyCaseModel.findOneAndUpdate(
    {
      userId: user._id,
      isDeleted: false,
    },
    {
      contactNumber,
      attachedImages,
      symptoms,
      otherInformation,
      alertness,
      alertnessLevel,
      patientNumber,
      urgencyLevel,
      acceptaceStatus,
      deliveringStatus,
      dateAndTimeConfirmArrival,
      latitude,
      longitude,
      assigneeName,
      assigneeNumber,
    },
    { new: true, omitUndefined: true }
  );
}

export async function softDeleteEmergencyCaseById(id) {
  if (isMongooseId(id)) {
    return await EmergencyCaseModel.findOneAndUpdate(
      {
        _id: id,
        isDeleted: false,
      },
      {
        isDeleted: true,
      },
      { new: true, omitUndefined: true }
    );
  } else {
    throw {
      message: "id is not valid",
      status: 404,
    };
  }
}
