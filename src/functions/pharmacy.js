import PharmacyModel from "../models/pharmacy.js";
import ProductModel from "../models/product.js";
import AddressModel from "../models/address.js";
import mongoose from "mongoose";
import { db } from "../loaders/firebase.js";
import {
  returnPharmacistById,
  returnPharmacistByUserId,
} from "./pharmacist.js";
import { returnUserById, returnUserByUid } from "./user.js";

const isMongooseId = mongoose.Types.ObjectId.isValid;

export async function createPharmacy(payload) {
  const { name, description } = payload;
  const {
    userId,
    hospitalId,
    pharmacistId,
    address,
    city,
    zipcode,
    latitude,
    longitude,
    type,
  } = payload;

  const createdAddress = await AddressModel.create({
    address,
    city,
    zipcode,
    type,
  });

  const pharmacist =
    (await returnPharmacistById(pharmacistId)) ??
    (() => {
      throw {
        message: `pharmacist not found from pharmacistId ${pharmacistId}`,
        status: 404,
      };
    });

  const user =
    (await returnUserById(pharmacist.userId)) ??
    (() => {
      throw {
        message: `user not found from pharmacist.userId ${pharmacist.userId}`,
        status: 404,
      };
    })();

  //store lat and long in firebase
  await createMapCollection(user.user.uid, latitude, longitude, city);

  const pharmacy = await PharmacyModel.create({
    name,
    description,
    userId,
    pharmacistId,
    addressId: createdAddress._id,
  });

  return { pharmacy, createdAddress, latitude, longitude, city };
}

export async function createMapCollection(uid, latitude, longitude, city) {
  const mapData = {
    pharmacy: {
      coordinate: {
        latitude,
        longitude,
      },
      city,
    },
  };

  const mapsRef = db.collection("maps").doc(uid);

  mapsRef
    .set(mapData)
    .then(() => {
      console.log("Document written with ID: ", uid);
    })
    .catch((error) => {
      console.error("Error adding document: ", error);
    });
}

export async function returnAllPharmacy() {
  return await PharmacyModel.find({
    isDeleted: false,
  });
}

export async function returnPharmacyById(id) {
  if (isMongooseId(id)) {
    return await PharmacyModel.findOne({
      _id: id,
      isDeleted: false,
    });
  } else {
    throw {
      message: "invalid id",
      status: 404,
    };
  }
}

export async function returnAllProductsById(id) {
  if (isMongooseId(id)) {
    return await ProductModel.find({
      pharmacyId: id,
      isDeleted: false,
    });
  } else {
    throw {
      message: "invalid id",
      status: 404,
    };
  }
}

export async function returnPharmacyByPharmacistId(pharmacistId) {
  if (isMongooseId(pharmacistId)) {
    const pharmacy = await PharmacyModel.find({
      pharmacistId: pharmacistId.toString(),
      isDeleted: false,
    });

    return pharmacy;
  } else {
    throw {
      message: "invalid id",
      status: 404,
    };
  }
}

export async function updatePharmacyById(payload, id) {
  const { name, description } = payload;
  if (isMongooseId(id)) {
    return await PharmacyModel.findOneAndUpdate(
      {
        _id: id,
        isDeleted: false,
      },
      {
        name,
        description,
      },
      {
        new: true,
        omitUndefined: true,
      }
    );
  } else {
    throw {
      message: "invalid id",
      status: 404,
    };
  }
}

export async function softDeletePharmacyById(id) {
  if (isMongooseId(id)) {
    return await PharmacyModel.findOneAndUpdate(
      {
        _id: id,
        isDeleted: false,
      },
      {
        isDeleted: true,
      },
      { new: true, omitUndefined: true }
    );
  } else {
    throw {
      message: "id is not valid",
      status: 404,
    };
  }
}

export async function returnAllLatLongOfPharmacies() {
  const mapsRef = db.collection("maps");
  const snapshot = await mapsRef.get();

  const latLongList = [];

  for (const doc of snapshot.docs) {
    const uid = doc.id;
    const user = await returnUserByUid(uid);
    if (!user) {
      throw {
        message: `user not found from uid: ${uid}`,
        status: 404,
      };
    }

    const pharmacist = await returnPharmacistByUserId(user.user._id);
    const pharmacy = await returnPharmacyByPharmacistId(pharmacist._id);

    const lat = doc.data().pharmacy.coordinate.latitude;
    const lng = doc.data().pharmacy.coordinate.longitude;
    const city = pharmacy.city;

    latLongList.push({
      uid,
      pharmacy: {
        pharmacy,
        coordinate: {
          latitude: lat,
          longitude: lng,
        },
        city,
      },
    });
  }

  return latLongList;
}

export async function returnAllLatLongOfPharmaciesByCity(city) {
  const mapsRef = db.collection("maps");
  const snapshot = await mapsRef.where("pharmacy.city", "==", city).get();

  const latLongList = [];

  for (const doc of snapshot.docs) {
    const uid = doc.id;
    const user = await returnUserByUid(uid);
    if (!user) {
      throw {
        message: `user not found from uid: ${uid}`,
        status: 404,
      };
    }
    const pharmacist = await returnPharmacistByUserId(user.user._id);
    const pharmacy = await returnPharmacyByPharmacistId(pharmacist._id);
    const lat = doc.data().pharmacy.coordinate.latitude;
    const lng = doc.data().pharmacy.coordinate.longitude;
    const city = pharmacy.city;

    latLongList.push({
      uid,
      pharmacy: {
        pharmacy,
        coordinate: {
          latitude: lat,
          longitude: lng,
        },
        city,
      },
    });
  }

  return latLongList;
}
