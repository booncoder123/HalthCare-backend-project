import OrderModel from "../models/order.js";
import ProductModel from "../models/product.js";
import MedicineModel from "../models/medicine.js";

import mongoose from "mongoose";
import { returnUserById, returnUserByUid } from "./user.js";
import { returnPharmacistById } from "./pharmacist.js";
import { returnMedicalInformationById } from "./medicalInformation.js";
import order from "../models/order.js";

const isMongooseId = mongoose.Types.ObjectId.isValid;

export async function createOrder(payload, pharmacistUid) {
  const { userUid, medicines, deliveryFee, status, total, jobId } = payload;

  const userProfile = await returnUserByUid(userUid);
  if (!userProfile) {
    throw {
      message: `user not found from user id ${userId}`,
      status: 404,
    };
  }

  const pharmacistProfile = await returnUserByUid(pharmacistUid);
  if (!pharmacistProfile) {
    throw {
      message: `pharmacist not found from  pharmacist id ${pharmacistId}`,
      status: 404,
    };
  }

  const userId = userProfile.user._id;
  const pharmacistId = pharmacistProfile.pharmacist._id;

  var createdMedicines = [];

  for (let i = 0; i < medicines.length; i++) {
    const medicine = medicines[i];
    const createMedicine = await MedicineModel.create({
      pharmacyId: medicine.pharmacyId,
      name: medicine.name,
      dosage: medicine.dosage,
      duration: medicine.duration,
      price: medicine.price,
    });
    createdMedicines.push(createMedicine);
  }

  const order = await OrderModel.create({
    userId,
    pharmacistId,
    jobId,
    deliveryFee,
    medicines: createdMedicines,
    status,
    total,
  });

  return { order, userProfile, pharmacistProfile };
}

export async function returnAllOrder(uid) {
  let orders = await OrderModel.aggregate([
    {
      $lookup: {
        from: "pharmacists",
        localField: "pharmacistId",
        foreignField: "_id",
        as: "pharmacistProfile",
      },
    },
    {
      $lookup: {
        from: "users",
        localField: "pharmacistProfile.userId",
        foreignField: "_id",
        as: "pharmacistUserProfile",
      },
    },
    {
      $lookup: {
        from: "medical informations",
        localField: "pharmacistUserProfile.medicalId",
        foreignField: "_id",
        as: "pharmacistMedicalInformation",
      },
    },
    {
      $unwind: {
        path: "$pharmacistProfile",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $unwind: {
        path: "$pharmacistUserProfile",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $unwind: {
        path: "$pharmacistMedicalInformation",
        preserveNullAndEmptyArrays: true,
      },
    },
  ]);

  return orders;
}

export async function returnOrderById(id) {
  if (isMongooseId(id)) {
    let order = await OrderModel.findOne({
      _id: id,
      isDeleted: false,
    });

    return order;
  } else {
    throw {
      message: "invalid id",
      status: 404,
    };
  }
}

async function formatMedicines(medicines) {
  const newMedicine = [];

  for (const medicine in medicines) {
    const medicineInformation = await MedicineModel.findOne(medicine._id);
    newMedicine.push(medicineInformation);
  }

  return newMedicine;
}

export async function returnDoneOrderByTokUserId(uid) {
  const user = await returnUserByUid(uid);

  if (!user) {
    throw {
      message: "user not found from given uid",
      status: 404,
    };
  }

  const orders = await OrderModel.aggregate([
    {
      $match: {
        userId: user.user._id,
        isDeleted: false,
      },
    },
    {
      $lookup: {
        from: "jobs",
        localField: "jobId",
        foreignField: "_id",
        as: "job",
      },
    },
    {
      $unwind: {
        path: "$job",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $match: {
        "job.status": "done",
      },
    },
  ]);

  if (!orders) {
    throw {
      message: `order is not found from user id: ${user.user._id}`,
      status: 404,
    };
  }

  let newOrder = [];

  for (let i = 0; i < orders.length; i++) {
    const pharmacist = await returnPharmacistById(orders[i].pharmacistId);
    const pharmacistProfile = await returnUserById(pharmacist.userId);

    newOrder.push({
      order: orders[i],
      pharmacistInfo: pharmacistProfile.medicalInformation,
    });
  }

  return { user, orders: newOrder };
}

export async function returnOrderByTokUserId(uid) {
  const user = await returnUserByUid(uid);

  if (!user) {
    throw {
      message: "user not found from given uid",
      status: 404,
    };
  }

  const orders = await OrderModel.find({
    userId: user.user._id,
    isDeleted: false,
  });

  if (!orders) {
    throw {
      message: `order is not found from user id: ${user.user._id}`,
      status: 404,
    };
  }

  let newOrder = [];

  for (let i = 0; i < orders.length; i++) {
    const pharmacist = await returnPharmacistById(orders[i].pharmacistId);
    const pharmacistProfile = await returnUserById(pharmacist.userId);

    newOrder.push({
      order: orders[i],
      pharmacistInfo: pharmacistProfile.medicalInformation,
    });
  }

  return { user, orders: newOrder };
}

export async function returnOrderByTokPharmacistId(uid) {
  const user = await returnUserByUid(uid);

  if (!user) {
    throw {
      message: "user not found from given uid",
      status: 404,
    };
  }

  if (!user.pharmacist) {
    throw {
      message: `user id: ${user.user._id} is not a pharmacist`,
      status: 404,
    };
  }

  const orders = await OrderModel.find({
    pharmacistId: user.pharmacist._id,
    isDeleted: false,
  });

  if (!orders) {
    throw {
      message: `order is not found from user id: ${user.user._id}`,
      status: 404,
    };
  }

  let newOrder = [];

  for (let i = 0; i < orders.length; i++) {
    const buyerInfo = await returnUserById(orders[i].userId);
    newOrder.push({
      order: orders[i],
      buyerInfo: buyerInfo.medicalInformation,
    });
  }

  return { user, orders: newOrder };
}

export async function returnDoneOrderByTokPharmacistId(uid) {
  const user = await returnUserByUid(uid);

  if (!user) {
    throw {
      message: "user not found from given uid",
      status: 404,
    };
  }

  if (!user.pharmacist) {
    throw {
      message: `user id: ${user.user._id} is not a pharmacist`,
      status: 404,
    };
  }

  const orders = await OrderModel.aggregate([
    {
      $match: {
        pharmacistId: user.pharmacist._id,
        isDeleted: false,
      },
    },
    {
      $lookup: {
        from: "jobs",
        localField: "jobId",
        foreignField: "_id",
        as: "job",
      },
    },
    {
      $unwind: {
        path: "$job",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $match: {
        "job.status": "done",
      },
    },
  ]);

  if (!orders) {
    throw {
      message: `order is not found from user id: ${user.user._id}`,
      status: 404,
    };
  }

  let newOrder = [];

  for (let i = 0; i < orders.length; i++) {
    const buyerInfo = await returnUserById(orders[i].userId);
    newOrder.push({
      order: orders[i],
      buyerInfo: buyerInfo.medicalInformation,
    });
  }

  return { user, orders: newOrder };
}

export async function returnOrderByUserId(id) {
  const user = await returnUserById(id);
  let order = await OrderModel.findOne({
    userId: user.user._id,
    isDeleted: false,
  });

  return { user, order };
}

export async function returnOrderByPharmacistId(id) {
  const pharmacist = await returnPharmacistById(id);
  if (!pharmacist) {
    throw {
      message: `pharmacist not found from pharmacist id ${pharmacistId}`,
      status: 404,
    };
  }

  const user = await returnUserById(pharmacist.userId);

  const userMedicalInformation = await returnMedicalInformationById(
    user.user.medicalId
  );

  if (!user) {
    throw {
      message: `user not found from pharmacist.userId ${pharmacist.userId}`,
      status: 404,
    };
  }

  let orders = await OrderModel.find({
    pharmacistId: id,
    isDeleted: false,
  });

  return { user, orders, pharmacist, userMedicalInformation };
}

export async function updateOrderById(payload, id) {
  const { userId, pharmacistId, deliveryFee, medicines, status, total } =
    payload;
  if (isMongooseId(id)) {
    return await OrderModel.findOneAndUpdate(
      {
        _id: id,
        isDeleted: false,
      },
      {
        userId,
        pharmacistId,
        medicines,
        deliveryFee,
        total,
        status,
      },
      {
        new: true,
        omitUndefined: true,
      }
    );
  } else {
    throw {
      message: "invalid id",
      status: 404,
    };
  }
}

export async function softDeleteOrderById(id) {
  if (isMongooseId(id)) {
    return await OrderModel.findOneAndUpdate(
      {
        _id: id,
        isDeleted: false,
      },
      {
        isDeleted: true,
      },
      { new: true, omitUndefined: true }
    );
  } else {
    throw {
      message: "id is not valid",
      status: 404,
    };
  }
}
