import MedicalStaffModel from "../models/medicalStaff.js";
import UserModel from "../models/user.js";
import MedicalInformationModel from "../models/medicalInformation.js";
import { returnUserByUid } from "../functions/user.js";

import mongoose from "mongoose";

const isMongooseId = mongoose.Types.ObjectId.isValid;

export async function createMedicalStaff(payload) {
  const { hospitalId, userId, role, licenseId, licenseExpireDate } = payload;

  return await MedicalStaffModel.create({
    hospitalId,
    userId,
    role,
    licenseId,
    licenseExpireDate,
  });
}

export async function returnAllMedicalStaff() {
  return await MedicalStaffModel.find({
    isDeleted: false,
  });
}

export async function returnMedicalStaffById(id) {
  if (isMongooseId(id)) {
    return await MedicalStaffModel.findOne({
      _id: id,
      isDeleted: false,
    });
  } else {
    throw {
      message: "invalid id",
      status: 404,
    };
  }
}

export async function returnMedicalStaffByHospitalId(hospitalId) {
  if (isMongooseId(hospitalId)) {
    let paramedics = await MedicalStaffModel.find({
      hospitalId: hospitalId,
      isOnDuty: true,
      isDeleted: false,
    });

    paramedics = JSON.parse(JSON.stringify(paramedics));
    for (let paramedic of paramedics) {
      let paramedicUser = await UserModel.findOne({ _id: paramedic.userId });
      paramedicUser = JSON.parse(JSON.stringify(paramedicUser));

      let paramedicMedicalInformation = await MedicalInformationModel.findOne({
        _id: paramedicUser.medicalId,
      });
      paramedicMedicalInformation = JSON.parse(
        JSON.stringify(paramedicMedicalInformation)
      );
      paramedic.name = paramedicMedicalInformation.name;
      paramedic.uid = paramedicUser.uid;
      paramedic.userId = paramedicUser._id;
    }
    return paramedics;
  } else {
    throw {
      message: "invalid id",
      status: 404,
    };
  }
}

export async function updateMedicalStaffById(payload, id) {
  const { hospitalId, userId, role, isOnDuty, licenseId, licenseExpireDate } =
    payload;
  if (isMongooseId(id)) {
    return await MedicalStaffModel.findOneAndUpdate(
      {
        _id: id,
        isDeleted: false,
      },
      {
        hospitalId,
        userId,
        role,
        licenseId,
        licenseExpireDate,
        isOnDuty,
      },
      {
        new: true,
        omitUndefined: true,
      }
    );
  } else {
    throw {
      message: "invalid id",
      status: 404,
    };
  }
}

export async function updateMedicalStaffByTok(uid, payload) {
  const { hospitalId, userId, role, isOnDuty, licenseId, licenseExpireDate } =
    payload;
  const user = await returnUserByUid(uid);
  try {
    const medicalStaffId = user.paramedics._id;

    const medicalStaff = await updateMedicalStaffById(
      { hospitalId, userId, role, isOnDuty, licenseId, licenseExpireDate },
      medicalStaffId
    );

    return medicalStaff;
  } catch (error) {
    throw {
      message: `user id ${user.user._id} is not paramedic ${error}`,
      status: 404,
    };
  }
}

export async function softDeleteMedicalStaffById(id) {
  if (isMongooseId(id)) {
    return await MedicalStaffModel.findOneAndUpdate(
      {
        _id: id,
        isDeleted: false,
      },
      {
        isDeleted: true,
      },
      { new: true, omitUndefined: true }
    );
  } else {
    throw {
      message: "id is not valid",
      status: 404,
    };
  }
}
