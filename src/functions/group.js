import mongoose from "mongoose";
import { db } from "../loaders/firebase.js";

export async function createGroupCollection(payload) {
  const { uid, jobId } = payload;
  const uid1 = uid[0];
  const uid2 = uid[1];

  const groupData = {
    member: uid,
    jobId,
    visible: true,
    lastMsg: {
      message: null,
      sendBy: null,
      sendAt: null,
    },
  };

  const groupsRef = db.collection("groups");

  groupsRef
    .get()
    .then((querySnapshot) => {
      let memberExists = false;
      querySnapshot.forEach((doc) => {
        const members = doc.data().member;
        if (members && members.includes(uid1) && members.includes(uid2)) {
          memberExists = true;
          console.log(
            `Members ${uid1} and ${uid2} already exist in document ${doc.id}`
          );

          // Update the document with {visible: true}
          groupsRef
            .doc(doc.id)
            .update({ visible: true })
            .then(() => {
              console.log("Document updated with {visible: true}");
            })
            .catch((error) => {
              console.error("Error updating document: ", error);
            });

          return;
        }
      });
      if (!memberExists) {
        groupsRef
          .add(groupData)
          .then((docRef) => {
            console.log("Document written with ID: ", docRef.id);
          })
          .catch((error) => {
            console.error("Error adding document: ", error);
          });
      }
    })
    .catch((error) => {
      console.log("Error getting documents: ", error);
    });
}

export async function readCollection() {
  db.collection("groups")
    .get()
    .then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data()}`);
      });
    });
}

export async function updateLastMessage(groupId, message, sendBy, sendAt) {
  if (!groupId || typeof groupId !== "string") {
    console.error(`Invalid groupId: ${groupId}`);
    return;
  }

  const groupRef = db.collection("groups").doc(groupId);

  try {
    await groupRef.update({
      lastMsg: {
        message,
        sendBy,
        sendAt,
      },
    });
    console.log(`Last message in group ${groupId} updated successfully`);
  } catch (error) {
    console.error(`Error updating last message in group ${groupId}:`, error);
  }
}
