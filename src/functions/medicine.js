import MedicineModel from "../models/medicine.js";

import mongoose from "mongoose";
import ProductModel from "../models/product.js";

const isMongooseId = mongoose.Types.ObjectId.isValid;

export async function createMedicine(payload) {
  const { pharmacyId, name, dosage, duration, price } = payload;

  const medicine = await MedicineModel.create({
    pharmacyId,
    name,
    dosage,
    duration,
    price,
  });

  return medicine;
}

export async function returnAllMedicine() {
  return await MedicineModel.find({
    isDeleted: false,
  });
}

export async function returnAllMedicineByPharmacyId(id) {
  return await MedicineModel.find({
    pharmacyId: id,
    isDeleted: false,
  });
}

export async function returnMedicineById(id) {
  if (isMongooseId(id)) {
    const medicine = await MedicineModel.findOne({
      _id: id,
      isDeleted: false,
    });

    return medicine;
  } else {
    throw {
      message: "invalid id",
      status: 404,
    };
  }
}

export async function updateMedicineById(payload, id) {
  const { pharmacyId, name, dosage, duration, price } = payload;
  if (isMongooseId(id)) {
    return await MedicineModel.findOneAndUpdate(
      {
        _id: id,
        isDeleted: false,
      },
      {
        pharmacyId,
        name,
        dosage,
        duration,
        price,
      },
      {
        new: true,
        omitUndefined: true,
      }
    );
  } else {
    throw {
      message: "invalid id",
      status: 404,
    };
  }
}

export async function softDeleteMedicineById(id) {
  if (isMongooseId(id)) {
    return await MedicineModel.findOneAndUpdate(
      {
        _id: id,
        isDeleted: false,
      },
      {
        isDeleted: true,
      },
      { new: true, omitUndefined: true }
    );
  } else {
    throw {
      message: "id is not valid",
      status: 404,
    };
  }
}

export async function searchMedicineByKeyword(keyword) {
  const products = await ProductModel.find({
    name: { $regex: keyword.toString().toLowerCase() },
  });

  return products.map((product) => product.name);
}
