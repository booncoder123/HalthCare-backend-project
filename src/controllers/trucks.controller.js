import { createCoordByUid } from "../functions/trucks.js";

import s3Controller from "../controllers/s3.controller.js";

export async function postCoordByUid(req, res, next) {
  try {
    //create user in mongoDB with provided detail from frontend
    const chat = await createCoordByUid(req.uid, req.body);

    res.send({ isOk: true, chat });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function postImages(req, res, next) {
  try {
    const { uid, files } = req;

    if (files) {
      const { images = [] } = files;
      
      const attachedImagesPath = await s3Controller.uploadFiles(
        images,
        uid,
        "attachedImages"
      );

      res.send({ isOk: true, attachedImagesPath });
    }
    else{
      throw{
        message: "No images found in input",
        status: 404
      }
    }
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}
