import {
  createJob,
  cancelJobByJobId,
  acceptJobByJobId,
  doneJobByJobId,
  returnAllJob,
  returnAllJobByPharmacistTok,
  returnPharmacistByJobId,
  returnJobRequesterById,
  returnJobReceiverById,
  increaseRadiusOfHospital,
  updateJobStatus,
} from "../functions/job.js";

export async function postJob(req, res, next) {
  try {
    const { uid } = req;
    const job = await createJob(req.body.type, uid, 3);

    res.send({ isOk: true, job });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function cancelJob(req, res, next) {
  try {
    const job = await cancelJobByJobId(req.body);

    res.send({ isOk: true, job });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function acceptJob(req, res, next) {
  try {
    const { uid } = req;
    const job = await acceptJobByJobId(req.body, uid);

    res.send({ isOk: true, job });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getAllJobByPharmacistTok(req, res, next) {
  try {
    const { uid } = req;
    const job = await returnAllJobByPharmacistTok(uid);

    res.send({ isOk: true, job });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getPharmacistByJobId(req, res, next) {
  try {
    const job = await returnPharmacistByJobId(req.params["id"]);
    res.send({ isOk: true, job });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getJobRequesterById(req, res, next) {
  try {
    const job = await returnJobRequesterById(req.params["id"]);

    res.send({ isOk: true, job });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getJobReceiverById(req, res, next) {
  try {
    const job = await returnJobReceiverById(req.params["id"]);

    res.send({ isOk: true, job });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function doneJob(req, res, next) {
  try {
    const job = await doneJobByJobId(req.body);

    res.send({ isOk: true, job });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getAllJob(req, res, next) {
  try {
    const job = await returnAllJob(req.body);
    res.send({ isOk: true, job });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function putIncreaseRadiusOfHospital(req, res, next) {
  try {
    const job = await increaseRadiusOfHospital(req.body);
    res.send({ isOk: true, job });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function putJobStatus(req, res, next) {
  try {
    const job = await updateJobStatus(req.body);
    res.send({ isOk: true, job });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}
