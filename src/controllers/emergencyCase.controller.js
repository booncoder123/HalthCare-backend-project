import {
  createEmergencyCase,
  returnAllEmergencyCase,
  returnEmergencyCaseById,
  returnEmergencyCaseByUid,
  returnEmergencyCaseByUserId,
  returnEmergencyCaseByHospitalId,
  returnEmergencyCaseAndOrderByTokUserId,
  returnEmergencyCaseByJobIdAndTok,
  softDeleteEmergencyCaseById,
  updateEmergencyCaseById,
  updateEmergencyCaseByUid,
  updateEmergencyCaseStatusById,
  returnEmergencyCaseAndJobById,
  returnEmergencyCaseAndMedInfoById,
  returnEmergencyCaseByJobId,
} from "../functions/emergencyCase.js";

import s3Controller from "../controllers/s3.controller.js";

export async function postEmergencyCase(req, res, next) {
  try {
    const { uid, files } = req;

    if (files) {
      const { images = [] } = files;

      const attachedImagesPath = await s3Controller.uploadFiles(
        images,
        uid,
        "attachedImages"
      );


      const emergencyCase = await createEmergencyCase(
        req.body,
        uid,
        attachedImagesPath,
      );
      res.send({
        isOk: true,
        data: emergencyCase,
    
      });
    } else {
      const emergencyCase = await createEmergencyCase(req.body, uid);
      res.send({
        isOk: true,
        data: emergencyCase,
      });
    }
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getEmergencyCaseById(req, res, next) {
  try {
    const emergencyCase = await returnEmergencyCaseById(req.params["id"]);
    res.send({ isOk: true, data: emergencyCase });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getEmergencyCaseAndJobById(req, res, next) {
  try {
    const emergencyCase = await returnEmergencyCaseAndJobById(req.params["id"]);
    res.send({ isOk: true, data: emergencyCase });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getEmergencyCaseByUserId(req, res, next) {
  try {
    const emergencyCase = await returnEmergencyCaseByUserId(
      req.params["userid"]
    );
    res.send({ isOk: true, data: emergencyCase });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getEmergencyCaseByUid(req, res, next) {
  try {
    const { uid } = req;
    const emergencyCase = await returnEmergencyCaseByUid(uid);
    res.send({ isOk: true, data: emergencyCase });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getEmergencyCaseByJobIdAndTok(req, res, next) {
  try {
    const { uid } = req;
    const emergencyCase = await returnEmergencyCaseByJobIdAndTok(
      uid,
      req.params["id"]
    );
    res.send({ isOk: true, data: emergencyCase });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getEmergencyCaseByHospitalId(req, res, next) {
  try {
    const emergencyCases = await returnEmergencyCaseByHospitalId(
      req.params["id"]
    );
    res.send({ isOk: true, data: emergencyCases });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getEmergencyCaseAndMedInfoById(req, res, next) {
  try {
    const { uid } = req;
    const emergencyCasesAndMedInfo = await returnEmergencyCaseAndMedInfoById(
      uid,
      req.params["id"]
    );
    res.send({ isOk: true, data: emergencyCasesAndMedInfo });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getEmergencyCaseByJobId(req, res, next) {
  try {
    const emergencyCases = await returnEmergencyCaseByJobId(
      req.params["jobId"]
    );
    res.send({ isOk: true, data: emergencyCases });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getEmergencyCaseAndOrderByTokUserId(req, res, next) {
  try {
    const { uid } = req;
    const emergencyCasesAndOrders =
      await returnEmergencyCaseAndOrderByTokUserId(uid);
    res.send({ isOk: true, data: emergencyCasesAndOrders });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getAllEmergencyCase(req, res, next) {
  try {
    const emergencyCases = await returnAllEmergencyCase();
    res.send({ isOk: true, data: emergencyCases });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function putEmergencyCaseById(req, res, next) {
  try {
    const id = req.params["id"];
    const emergencyCase = await updateEmergencyCaseById(req.body, id);
    res.send({ isOk: true, data: emergencyCase });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function putEmergencyCaseByUid(req, res, next) {
  try {
    const { uid } = req;
    const emergencyCase = await updateEmergencyCaseByUid(req.body, uid);
    res.send({ isOk: true, data: emergencyCase });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function putEmergencyCaseStatusById(req, res, next) {
  try {
    const emergencyCase = await updateEmergencyCaseStatusById(
      req.body,
      req.params["id"]
    );
    res.send({ isOk: true, data: emergencyCase });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function deleteEmergencyCaseById(req, res, next) {
  try {
    const id = req.params["id"];
    const emergencyCase = await softDeleteEmergencyCaseById(id);
    res.send({ isOk: true, data: emergencyCase });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}
