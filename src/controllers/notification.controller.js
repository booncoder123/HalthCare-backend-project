import axios from "axios";
import Notification from "../models/notification.model.js";
import NotificationToken from "../models/notificationToken.model.js";
import MedicalInformationModel from "../models/medicalInformation.js";
import fetch from "node-fetch";

import User from "../models/user.js";
import { db } from "../loaders/firebase.js";

export async function createNotification(req, res, next) {
  const session = await Notification.startSession();
  session.startTransaction();

  try {
    const { type, detail, title, body, otherId, userId } = req.body;
    const { files, uid } = req;

    // TODO ADD FIND USER BY ID
    const user = await User.findOne({ uid });

    if (!user) {
      const error = new Error("User not found.");
      error.statusCode = 404;
      throw error;
    }

    const report = new Notification({
      type,
      detail,
      uid,
      title,
      body,
      otherId,
      userId,
    });

    await report.save();

    await session.commitTransaction();
    session.endSession();

    res
      .status(201)
      .json({ ok: true, message: "created report successfully", data: report });
  } catch (err) {
    await session.abortTransaction();
    session.endSession();

    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
}

export async function getNotification(req, res, next) {
  try {
    const { userId } = req.params;

    const notification = await Notification.find({
      userId,
      isDelete: false,
    }).sort({ createdAt: -1 });

    res.status(200).json({
      ok: true,
      message: "get notification successfully",
      notification,
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
}

export async function readNotification(req, res, next) {
  const { id } = req.body;

  try {
    const notification = await Notification.findOneAndUpdate(
      { _id: id },
      { isRead: true }
    );

    res.status(200).json({
      ok: true,
      message: "read notification successfully",
      data: notification,
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
}

export async function deleteNotification(req, res, next) {
  const { id } = req.body;

  try {
    const notification = await Notification.findOneAndUpdate(
      { _id: id },
      { isDelete: true }
    );

    res.status(200).json({
      ok: true,
      message: "Delete notification successfully",
      data: notification,
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
}

export async function getNotificationById(req, res, next) {
  try {
    const { id } = req.params;

    const notification = await Notification.find({ userId: id }).lean();

    res.status(200).json({
      ok: true,
      message: "get notification successfully",
      notification,
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
}

export async function createNotificationToken(req, res, next) {
  const session = await NotificationToken.startSession();
  session.startTransaction();

  const { token, userId, uid } = req.body;

  try {
    const notificationsRef = db.collection("notifications").doc(uid);

    await notificationsRef.set(
      {
        [id1]: {
          [id2]: true,
        },
      },
      { merge: true }
    );

    return res
      .status(200)
      .json({ message: "Notification token created successfully" });

    // let notificationToken;

    // const notificationTokenExisted = await NotificationToken.findOne({ token });

    // if (notificationTokenExisted) {
    //   const error = new Error('Token already exists.');
    //   error.statusCode = 400;
    //   throw error;
    // }

    // if (userId) {
    //   const user = await User.findById(userId);

    //   if (!user) {
    //     const error = new Error('User not found.');
    //     error.statusCode = 404;
    //     throw error;
    //   }

    //   notificationToken = new NotificationToken({
    //     token,
    //     userId,
    //   });
    // } else {
    //   notificationToken = new NotificationToken({
    //     token,
    //   });
    // }

    // await notificationToken.save();

    // res.status(201).json({ ok: true, message: 'created notification token successfully', data: notificationToken });
  } catch (err) {
    await session.abortTransaction();
    session.endSession();

    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
}

export async function updateNotificationToken(req, res, next) {
  const session = await NotificationToken.startSession();
  session.startTransaction();

  try {
    const { token, userId } = req.body;

    const notificationToken = await NotificationToken.findOne({ token });

    if (!notificationToken) {
      const error = new Error("Notification token not found.");
      error.statusCode = 404;
      throw error;
    }

    notificationToken.token = token;

    if (userId) {
      notificationToken.userId = userId;
    } else {
      notificationToken.userId = null;
    }

    await notificationToken.save();

    res.status(200).json({
      ok: true,
      message: "update notification token successfully",
      data: notificationToken,
    });
  } catch (err) {
    await session.abortTransaction();
    session.endSession();

    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
}

export async function serverSendNotificationToUser(req, res, next) {
  const { uid, title, body, feature, groupId, type } = req.body;
  const { receiver } = req.params;

  try {
    const mapsRef = db.collection("notification").doc(receiver);

    const doc = await mapsRef.get();

    const user = await User.findOne({
      uid: receiver,
    });

    const { _id } = user;

    const notification = new Notification({
      userId: _id,
      title,
      body,
      feature,
      groupId,
      type,
    });
    await notification.save();

    const expoNotificationBody = {
      to: doc.data().tokens,
      title,
      body,
      data: {
        groupId,
        feature,
      },
      sound: "default",
      ios: {
        // optional configuration for iOS
        sound: true, // enable sound on iOS
      },
      android: {
        // optional configuration for Android
        channelId: "default",
        sound: true, // enable sound on Android
        priority: "max",
        group: "pecgo",
      },
    };

    const response = await fetch("https://exp.host/--/api/v2/push/send", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "accept-encoding": "gzip, deflate",
        host: "exp.host",
        "accept-language": "en-US,en;q=0.9",
      },
      body: JSON.stringify(expoNotificationBody),
    });

    res
      .status(200)
      .json({ ok: true, message: "send notification successfully", response });
  } catch (error) {
    console.log({ error });
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
}

export async function userSendNotificationToUser(req, res, next) {
  const { uid, title, body, feature, groupId } = req.body;
  const { receiver, sender } = req.params;
  console.log(req.body);

  const other = await User.findOne({
    uid: sender,
  });
  const { medicalId } = other;
  const otherData = await MedicalInformationModel.findOne({
    _id: medicalId,
  });
  const { name } = otherData;

  try {
    const mapsRef = db.collection("notification").doc(receiver);

    const doc = await mapsRef.get();

    const expoNotificationBody = {
      to: doc.data().tokens,
      title: name,
      sender,
      body,
      data: {
        groupId,
        feature,
      },
      sound: "default",
      ios: {
        // optional configuration for iOS
        sound: true, // enable sound on iOS
      },
      android: {
        // optional configuration for Android
        channelId: "default",
        sound: true, // enable sound on Android
        priority: "max",
        group: "pecgo",
      },
    };

    const response = await fetch("https://exp.host/--/api/v2/push/send", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "accept-encoding": "gzip, deflate",
        host: "exp.host",
        "accept-language": "en-US,en;q=0.9",
      },
      body: JSON.stringify(expoNotificationBody),
    });

    res
      .status(200)
      .json({ ok: true, message: "send notification successfully", response });
  } catch (error) {
    console.log({ error });
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
}

export async function getNotificationByUserId(req, res, next) {
  try {
    const { uid } = req;

    const { limit, page } = req.body;

    const user = await User.findOne({
      uid,
    });

    if (!user) {
      const error = new Error("User not found.");
      error.statusCode = 404;
      throw error;
    }

    const { _id } = user;

    const totalDocuments = await Notification.find({
      userId: _id,
    }).countDocuments();

    const totalPages = Math.ceil(totalDocuments / limit);

    const notification = await Notification.find({
      userId: _id,
    })
      .sort({ createdAt: -1 })
      .skip((page - 1) * limit)
      .limit(limit);

    res.status(200).json({
      isOk: true,
      message: "get notification successfully",
      notification,
      totalDocuments,
      totalPages,
      limit,
      page,
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
}
