import {
  createUser,
  returnAllUser,
  returnUserById,
  returnUserByUid,
  softDeleteUserById,
  updateUserById,
} from "../functions/user.js";

import s3Controller from "../controllers/s3.controller.js";

export async function postUser(req, res, next) {
  try {
    const { uid, files } = req;

    if (files) {
      const { faceImg } = files;
      if (faceImg) {
        const faceImgPath = await s3Controller.uploadFiles(
          faceImg,
          uid,
          "faceImg"
        );
        const user = await createUser(req.body, uid, faceImgPath);
        res.send({ isOk: true, data: user, faceImgPath });
      }
    } else {
      const user = await createUser(req.body, uid);
      res.send({ isOk: true, data: user });
    }
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getAllUser(req, res, next) {
  try {
    const users = await returnAllUser();
    res.send({ isOk: true, data: users });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getUserById(req, res, next) {
  try {
    const user = await returnUserById(req.params["id"]);
    res.send({ isOk: true, data: user });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getUserByUid(req, res, next) {
  try {
    const uid = req.uid ? req.uid : req.params["uid"];
    const user = await returnUserByUid(uid);
    res.send({ isOk: true, data: user });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function putUserById(req, res, next) {
  try {
    const { uid, files } = req;

    if (files) {
      const { faceImg = [] } = files;
      const attachedImagePath = await s3Controller.uploadFiles(
        faceImg,
        uid,
        "faceImg"
      );

      const user = await updateUserById(req.body, uid, attachedImagePath[0]);

      res.send({ isOk: true, data: user });
    } else {
      const user = await updateUserById(req.body, uid);
      res.send({
        isOk: true,
        data: user,
      });
    }
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function deleteUserById(req, res, next) {
  try {
    const id = req.params["id"];
    const user = await softDeleteUserById(id);
    res.send({ isOk: true, data: user });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}
