import { sendMessage, readCollection } from "../functions/message.js";

export async function createMessage(req, res, next) {
  try {
    //create user in mongoDB with provided detail from frontend
    const chat = await sendMessage(req.body);

    res.send({ isOk: true, chat });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function readMessage(req, res, next) {
  try {
    //create user in mongoDB with provided detail from frontend
    const chat = await readCollection(req.body);
    res.send({ isOk: true, chat });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}
