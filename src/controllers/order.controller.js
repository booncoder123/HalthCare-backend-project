import {
  createOrder,
  returnAllOrder,
  returnOrderById,
  returnOrderByTokUserId,
  returnOrderByTokPharmacistId,
  returnOrderByUserId,
  updateOrderById,
  softDeleteOrderById,
  returnOrderByPharmacistId,
} from "../functions/order.js";

export async function postOrder(req, res, next) {
  try {
    const { uid } = req;
    const order = await createOrder(req.body, uid);
    res.send({ isOk: true, data: order });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getOrderById(req, res, next) {
  try {
    const order = await returnOrderById(req.params["id"]);
    res.send({ isOk: true, data: order });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getOrderByTokUserId(req, res, next) {
  try {
    const { uid } = req;
    const order = await returnOrderByTokUserId(uid);
    res.send({ isOk: true, data: order });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getOrderByTokPharmacistId(req, res, next) {
  try {
    const { uid } = req;
    const order = await returnOrderByTokPharmacistId(uid);
    res.send({ isOk: true, data: order });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getOrderByUserId(req, res, next) {
  try {
    const order = await returnOrderByUserId(req.params["id"]);
    res.send({ isOk: true, data: order });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getOrderByPharmacistId(req, res, next) {
  try {
    const order = await returnOrderByPharmacistId(req.params["id"]);
    res.send({ isOk: true, data: order });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getAllOrder(req, res, next) {
  try {
    const { uid } = req;
    const orders = await returnAllOrder(uid);
    res.send({ isOk: true, data: orders });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function putOrderById(req, res, next) {
  try {
    const id = req.params["id"];
    const order = await updateOrderById(req.body, id);
    res.send({ isOk: true, data: order });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function deleteOrderById(req, res, next) {
  try {
    const id = req.params["id"];
    const order = await softDeleteOrderById(id);
    res.send({ isOk: true, data: order });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}
