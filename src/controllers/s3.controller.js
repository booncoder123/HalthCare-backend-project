import path from "path";
import fs from "fs";
import { v4 as uuidv4 } from "uuid";
import { packageDirectory } from "pkg-dir";
import aws from "../loaders/s3.js";
import config from "../config/index.js";
import sharp from "sharp";

const s3 = new aws.S3();

async function uploadFiles(images, userId, pathS3) {
  const projectRoot = (await packageDirectory()) || process.cwd();
  try {
    const fileReturn = await Promise.all(
      images.map(async (item) => {
        const filePath = path.join(projectRoot, "temp", item.filename);
        const key = `${pathS3}/${userId}/${item.filename}${uuidv4()}.jpeg`;
        const params = {
          Bucket: config.aws.bucketName,
          Key: key,
          Body: fs.createReadStream(filePath),
          ContentType: "image/jpeg",
        };

        await sharp(filePath)
          // .resize({ width: desiredWidth, height: desiredHeight })
          .jpeg({ quality: 60 })
          .toBuffer()
          .then((buffer) => {
            params.Body = buffer;
          });

        const data = await s3
          .upload(params)
          .promise()
          .then(async (tempData) => {
            await fs.unlinkSync(filePath);
            return tempData;
          });
        return data.key;
      })
    );

    return fileReturn;
  } catch (error) {
    console.error("Error occured while trying to upload to S3 bucket", error);
    throw error;
  }
}

const deleteFiles = async (Key) => {
  let keyArray = Key;
  if (!Array.isArray(Key)) {
    keyArray = Array.from(keyArray);
  }
  try {
    await Promise.all(
      keyArray.map(async (item) => {
        const params = {
          Bucket: config.aws.bucketName,
          Key: item,
        };

        await s3.deleteObject(params).promise();
      })
    );
    return true;
  } catch (error) {
    console.error("Error occured while trying to delete to S3 bucket", error);
    throw error;
  }
};

const uploadFile = async (file, userId, pathS3) => {
  const projectRoot = (await packageDirectory()) || process.cwd();
  try {
    const filePath = path.join(projectRoot, "temp", file.filename);
    const key = `${pathS3}/${userId}/${file.filename}${uuidv4()}.jpeg`;
    const params = {
      Bucket: config.aws.bucketName,
      Key: key,
      Body: fs.createReadStream(filePath),
      ContentType: "image/jpeg",
      // ACL: 'public-read',
    };

    const data = await s3

      .upload(params)
      .promise()
      .then(async (tempData) => {
        await fs.unlinkSync(filePath);
        return tempData;
      });
    return data.key;
  } catch (error) {
    console.error("Error occured while trying to upload to S3 bucket", error);
    throw error;
  }
};

const deleteFile = async (Key) => {
  try {
    const params = {
      Bucket: config.aws.bucketName,
      Key,
    };

    await s3.deleteObject(params).promise();

    return true;
  } catch (error) {
    console.error("Error occured while trying to delete to S3 bucket", error);
    throw error;
  }
};

export default {
  uploadFiles,
  deleteFiles,
  uploadFile,
  deleteFile,
};
