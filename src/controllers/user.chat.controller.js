import {
  createUserCollection,
  readCollection,
} from "../functions/user.chat.js";

export async function createUser(req, res, next) {
  try {
    //create user in mongoDB with provided detail from frontend
    const chat = await createUserCollection(req.body);

    res.send({ isOk: true, chat });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function readUser(req, res, next) {
  try {
    //create user in mongoDB with provided detail from frontend
    const chat = await readCollection(req.body);
    res.send({ isOk: true, chat });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}
