import {
  createMedicine,
  returnAllMedicine,
  returnMedicineById,
  returnAllMedicineByPharmacyId,
  updateMedicineById,
  softDeleteMedicineById,
  searchMedicineByKeyword,
} from "../functions/medicine.js";

export async function postMedicine(req, res, next) {
  try {
    const medicine = await createMedicine(req.body);
    res.send({ isOk: true, data: medicine });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getMeidicineById(req, res, next) {
  try {
    const medicine = await returnMedicineById(req.params["id"]);
    res.send({ isOk: true, data: medicine });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getAllMedicine(req, res, next) {
  try {
    const medicines = await returnAllMedicine();
    res.send({ isOk: true, data: medicines });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function getAllMedicineByPharmacyId(req, res, next) {
  try {
    const medicines = await returnAllMedicineByPharmacyId(req.params["id"]);
    res.send({ isOk: true, data: medicines });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function putMedicineById(req, res, next) {
  try {
    const id = req.params["id"];
    const medicine = await updateMedicineById(req.body, id);
    res.send({ isOk: true, data: medicine });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function deleteMedicineById(req, res, next) {
  try {
    const id = req.params["id"];
    const medicine = await softDeleteMedicineById(id);
    res.send({ isOk: true, data: medicine });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function searchMedicine(req, res, next) {
  try {
    const medicine = await searchMedicineByKeyword(req.params["keyword"]);
    res.send({ isOk: true, data: medicine });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}
