import { createGroupCollection, readCollection } from "../functions/group.js";

export async function createGroup(req, res, next) {
  try {
    //create user in mongoDB with provided detail from frontend
    const chat = await createGroupCollection(req.body);

    res.send({ isOk: true, chat });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function readGroup(req, res, next) {
  try {
    //create user in mongoDB with provided detail from frontend
    const chat = await readCollection(req.body);
    res.send({ isOk: true, chat });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}
