import { createUser, checkEmailExist } from "../functions/user.js";

export async function register(req, res, next) {
  try {
    //create user in mongoDB with provided detail from frontend
    const user = await createUser(req.body);
    res.send({ isOk: true, data: user });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}

export async function emailExist(req, res, next) {
  try {
    const user = await checkEmailExist(req.body.email);
    res.send({ isOk: true, data: user });
  } catch (error) {
    console.log("error: ", error);
    next({ message: error.message, status: error.status });
  }
}
