import Bull from "bull";

// import dotenv from "dotenv";

// if (process.env.NODE_ENV === "development") {
//   dotenv.config({ path: ".env.development" });
// } else if (process.env.NODE_ENV === "production") {
//   dotenv.config({ path: ".env.production" });
// } else {
//   dotenv.config();
// }

if (process.error) {
  throw new Error("⚠️  Couldn't find .env file  ⚠️");
}

const connectQueue = (name) =>
  new Bull(name, {
    redis: { port: process.env.REDIS_PORT, host: process.env.REDIS_HOST },
  });

export default connectQueue;
