import aws from "aws-sdk";
import config from "../config/index.js";

aws.config.setPromisesDependency();
aws.config.update({
  accessKeyId: config.aws.acesskeyId,
  secretAccessKey: config.aws.secretAcessKeyId,
  region: config.aws.region,
});

export default aws;
